// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.2 (win64) Build 1577090 Thu Jun  2 16:32:40 MDT 2016
// Date        : Tue Oct 24 17:52:36 2017
// Host        : pcepesedesk02 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vivado_debug/ip/xlx_k7v7_vivado_debug/xlx_k7v7_vivado_debug_stub.v
// Design      : xlx_k7v7_vivado_debug
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ila,Vivado 2016.2" *)
module xlx_k7v7_vivado_debug(clk, probe0, probe1, probe2, probe3)
/* synthesis syn_black_box black_box_pad_pin="clk,probe0[7:0],probe1[7:0],probe2[7:0],probe3[7:0]" */;
  input clk;
  input [7:0]probe0;
  input [7:0]probe1;
  input [7:0]probe2;
  input [7:0]probe3;
endmodule
