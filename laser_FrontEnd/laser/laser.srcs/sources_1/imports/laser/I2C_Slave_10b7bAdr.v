///////////////////////////////////////////////////////////////////////////////
// Company:        CERN (PH-ESE Dep.)                                        //
// Engineer:       Alessandro Caratelli (caratelli.a@gmail.com)              //
//                                                                           //
// Create Date:    14:35:55 04/11/2013                                       //
// Design Name:    GBT-SCA Test system                                       //
// Module Name:    I2C_slave_7b10bAdr                                        //
// Project Name:   GBT-SCA Test system                                       //
// Target Devices: Xilinx Spartan-6 FPGA                                     //
// Language:       Verilog                                                   //
// Description:    General Purpose I2C slave, for 7bit and 10bit addressing. //
//                 This version implement autoincrement register address     //
//                 instead random register write                             //
//                                                                           //
// Dependencies:                                                             //
// Revision:                                                                 //
// Revision        0.01 - File Created                                       //
// Additional Comments: Use a clock frequence at least 5 times faster then   //
//                      the SCL signal to have best results                  //
///////////////////////////////////////////////////////////////////////////////


`timescale 1ns / 1ps

module I2C_slave_7b10bAdr # (
	parameter     I2C_10b_ADR = 10'h033,
      parameter     I2C_7b_ADR  = 7'h33, 
      parameter     MEM_DIM     = 4
      )(
      input         clk,
      input         resetb, //active low
      input         SCL,
      input         SDA,
      output reg    SDAout,
      output reg    [(8*MEM_DIM-1):0] I2CDataRec,
      input         [(8*MEM_DIM-1):0] I2CDataSend, 
      output reg    SetDataValid //To tall to the softwere that there are new data.
      ); 

      `define idle                   4'h0
	`define st0                    4'h1
	`define st1                    4'h2
	`define read_cycleA            4'h3
	`define read_cycleB            4'h4
	`define send_ackA              4'h5
	`define send_ackB              4'h6
	`define write_cycleA           4'h7
	`define write_cycleB           4'h8
	`define ack_nA                 4'h9
	`define ack_nB                 4'ha
	`define set_data_valid         4'hb
	`define rep_start              4'hc

      reg       [3:0]  state, snext;
      reg       [6:0]  seq,seqd;
      reg       [2:0]  cyclecount, cyclecount_d;
      reg       [7:0]  register, register_d;
      reg       [14:0] address;
      reg       [7:0]  mem    [0:(MEM_DIM-1'b1)];
      wire      [7:0]  mem_up [0:(MEM_DIM-1'b1)];
      reg       [1:0]  sm;
      reg              str, str_d;
      reg              is10b;
      reg              r_w;    
        
      wire      [(8*MEM_DIM-1):0] mem_dw;


   always @ (posedge clk) begin
            if(~resetb) begin : initialize
                  integer i;
                  state      <= `idle;
                  cyclecount <= 3'h0;
                  register   <= 8'h00;
                  address    <= 15'h0000;
                  seq        <= 7'h00;
                  r_w        <= 1'b1;
                  sm         <= 2'b00;
                  str        <= 2'b00;
                  is10b      <= 1'b0;
                  for (i=0;i<MEM_DIM;i=i+1) mem[{i}] <= 8'h00;
                  end
            else begin : statemachine
                  register <= register_d;
                  seq <= seqd;
                  str <= str_d;
                  cyclecount <= cyclecount_d;
                  if(state==`send_ackA) case(seq)
                              0: begin
                                    address[14:8] <= register[7:1];  //Reading first part of adress
                                    if(register[7:3] == {{4{1'b1}},1'b0}) begin
                                          is10b <= 1'b1;
                                          //$display("Is 10bit addressing");
                                          end
                                    else begin
                                          is10b <= 1'b0;
                                          r_w <= ~register[0];
                                          end
                                 end
                              1: begin
                                    if (is10b) address[7:0] <= register[7:0]; //Reading second part of adress
                                       else mem[0] <= register[7:0];
                                 end
                              2: begin
                                    if(is10b)
                                          if(register == {5'b11110,I2C_10b_ADR[9:8],1'b1}) r_w <= 1'b0;
                                          else begin
                                                r_w <= 1'b1;
                                                mem[0] <= register[7:0];
                                                end
                                    else begin
                                          mem[1] <= register[7:0];                                          
                                          end
                                 end
                              default: begin                                                
                                    case({is10b,r_w}) 
                                          2'b11: if((seq-4'h2)<=(MEM_DIM-4'h1)) //reading data (master to slave transmission)
                                                            mem[(seq-4'h2)][7:0] <= register[7:0];
                                          2'b01: if((seq-4'h1)<=(MEM_DIM-4'h1)) //reading data (master to slave transmission)
                                                            mem[(seq-4'h1)][7:0] <= register[7:0];
                                    endcase
                                 end
                        endcase
                  
                  case(sm)//detecting of stop condition
                        2'b00: begin 
                                     if(~SDA & ~SCL) sm<=2'b01; 
                                     state <= snext; 
                               end
                        2'b01: begin
                                     if(~SDA & SCL) sm<=2'b10;
                                     else if(SDA) sm<=2'b00;
                                     state <= snext; 
                               end
                        2'b10: begin
                                     if(SDA & SCL) begin //stop condition detected
                                          state <= `set_data_valid; 
                                          sm <= 2'b00;
                                          end
                                     else begin 
                                          if(~SCL) sm<=2'b00;
                                          state <= snext;
                                          end
                               end
                        default: begin 
                                    sm<=2'b00;  
                                    state <= snext; 
                                  end
                  endcase
                 
              end      
      end
      // KHW change for Quartus!
		/*
      genvar i;
      for(i=0;i<MEM_DIM;i=i+1) begin : MemAssignament
            assign mem_dw[(7+i*8):(i*8)] = (mem[i][7:0]);
            assign mem_up[MEM_DIM-i-1][7:0] = I2CDataSend[(7+i*8):(i*8)];
      end
		*/
		
		assign mem_dw[7:0] = mem[0][7:0];
      assign mem_dw[15:8] = mem[1][7:0];
      assign mem_dw[23:16] = mem[2][7:0];
      assign mem_dw[31:24] = mem[3][7:0];
//      assign mem_dw[39:32] = mem[4][7:0];
//      assign mem_dw[47:40] = mem[5][7:0];
//      assign mem_dw[55:48] = mem[6][7:0];
//      assign mem_dw[63:56] = mem[7][7:0];
//      assign mem_dw[71:64] = mem[8][7:0];
//      assign mem_dw[79:72] = mem[9][7:0];
//      assign mem_dw[87:80] = mem[10][7:0];
//      assign mem_dw[95:88] = mem[11][7:0];
//      assign mem_dw[103:96] = mem[12][7:0];
//      assign mem_dw[111:104] = mem[13][7:0];
//      assign mem_dw[119:112] = mem[14][7:0];
//      assign mem_dw[127:120] = mem[15][7:0];
//      assign mem_up[15][7:0] = I2CDataSend[7:0];
//      assign mem_up[14][7:0] = I2CDataSend[15:8];
//      assign mem_up[13][7:0] = I2CDataSend[23:16];
//      assign mem_up[12][7:0] = I2CDataSend[31:24];
//      assign mem_up[11][7:0] = I2CDataSend[39:32];
//      assign mem_up[10][7:0] = I2CDataSend[47:40];
//      assign mem_up[9][7:0]  = I2CDataSend[55:48];
//      assign mem_up[8][7:0]  = I2CDataSend[63:56];
//      assign mem_up[7][7:0]  = I2CDataSend[71:64];
//      assign mem_up[6][7:0]  = I2CDataSend[79:72];
//      assign mem_up[5][7:0]  = I2CDataSend[87:80];
//      assign mem_up[4][7:0]  = I2CDataSend[95:88];
//      assign mem_up[3][7:0]  = I2CDataSend[103:96];
//      assign mem_up[2][7:0]  = I2CDataSend[111:104];
//      assign mem_up[1][7:0]  = I2CDataSend[119:112];
//      assign mem_up[0][7:0]  = I2CDataSend[127:120];

//      assign mem_up[7][7:0]  = I2CDataSend[63:56];
//      assign mem_up[6][7:0]  = I2CDataSend[55:48];
//      assign mem_up[5][7:0]  = I2CDataSend[47:40];
//      assign mem_up[4][7:0]  = I2CDataSend[39:32];
      assign mem_up[3][7:0]  = I2CDataSend[31:24];
      assign mem_up[2][7:0]  = I2CDataSend[23:16];
      assign mem_up[1][7:0]  = I2CDataSend[15:8];
      assign mem_up[0][7:0]  = I2CDataSend[7:0];
      
      // end of KHW change
      always @ (posedge clk) begin
            if(~resetb) 
                  I2CDataRec <= 0;
            else
                  if((state == `set_data_valid) && r_w) 
                        I2CDataRec <= mem_dw;
                  else 
                        I2CDataRec <= I2CDataRec;
      end
      

      always @(*) begin
            //default outputs
            cyclecount_d = cyclecount;
            SDAout=1'b1;
            seqd=seq;
            snext = state;
            register_d = register;
            SetDataValid = 1'b0;
            //state machine
            if(state != `read_cycleB) str_d = 1'b0;
            case(state)
                  `idle: begin
                              snext = (SDA && SCL) ? `st0 : `idle;
                              register_d=0;
                              cyclecount_d=0;
                              seqd=0;
                              end            
                  `st0: begin
                              case({SDA,SCL})
                                    2'b01: snext = `st1;
                                    2'b11: snext = state;
                                    default: snext = `idle;
                              endcase
                              register_d=0;
                              cyclecount_d=0;
                              seqd=0;
                              end            
                  `st1:      case({SDA,SCL})
                                    2'b00: snext = `read_cycleA;
                                    2'b01: snext = state;
                                    default: snext = `idle;
                              endcase
                  `read_cycleA: snext = (SCL) ? `read_cycleB : state;
                  `read_cycleB: begin
                                    register_d[7:0]=register[7:0];
                                    register_d[7-cyclecount]=SDA;
                                    case({SCL,SDA})
                                    2'b01,
                                    2'b00: begin 
                                                str_d = 1'b0;
                                                if(cyclecount<7) begin
                                                      snext = `read_cycleA;
                                                      cyclecount_d = cyclecount+1'b1;
                                                end
                                                else begin
                                                      snext = `send_ackA;
                                                      cyclecount_d = 0;
                                                end
                                           end
                                    2'b11: begin
                                                str_d = 1'b1;       
                                                snext = state;
                                           end
                                    2'b10: begin 
                                    		snext = state;
                                    		str_d = 1'b0;
                                                if(str) snext = `rep_start;
                                                else    snext =  state;
                                           end
                                   endcase
                              end
                  `rep_start: begin
                  			cyclecount_d = 3'h0;
                  			if(~SCL) snext = `read_cycleA;
                  			else snext = state;
                  		end
                  `send_ackA: begin
                              SDAout = 1'b0;
                              if(SCL) begin
                                    snext = `send_ackB;
                                    seqd = seq+4'b1;
                                    end
                              else snext = state;
                              end
                  `send_ackB: begin
                              SDAout = 1'b0; 
                              if(~SCL) begin
                                    case(seq)
                                          0: snext = `idle; //error
                                          1: if (~is10b)
                                                      if(address[14:8] == I2C_7b_ADR)
                                                            snext= (~r_w) ? `write_cycleA : `read_cycleA;
                                                      else snext = `idle;
                                                else snext = `read_cycleA;
                                          2: if(is10b)
                                                      if((address[14:10] == 5'b11110) && (address[9:0] == I2C_10b_ADR)) snext=`read_cycleA;
                                                      else snext = `idle;
                                                else snext=`read_cycleA;
                                          3: if(is10b)      
                                                      if((address[14:10] == 5'b11110) && (address[9:0] == I2C_10b_ADR)) 
                                                            snext= (~r_w) ? `write_cycleA : `read_cycleA;
                                                      else
                                                            snext = `idle;
                                                else snext=`read_cycleA;
                                          default: begin 
                                                snext= (~r_w) ? `write_cycleA : `read_cycleA;
                                                end
                                          endcase 
                                    end
                              end
                  `write_cycleA: begin
                              snext = (SCL) ? `write_cycleB : state;
                              SDAout = (is10b) ? mem_up[seq-3][7-cyclecount] : mem_up[seq-1][7-cyclecount];
                              end
                  `write_cycleB: begin
                              SDAout = (is10b) ? mem_up[seq-3][7-cyclecount] : mem_up[seq-1][7-cyclecount];
                              if(~SCL) 
                                    if(cyclecount<7) begin
                                          snext = `write_cycleA;
                                          cyclecount_d = cyclecount+1'b1;
                                          end
                                    else begin
                                          snext = `ack_nA;
                                          cyclecount_d = 0;
                                          end
                              else snext = state;
                              end
                  `ack_nA:begin
                              SDAout = 1'b1;
                              case({SDA,SCL})
                                    2'b00,2'b10: snext = state;
                                    2'b01: begin
                                                 snext = `ack_nB;
                                                 seqd = seq+4'b1;
                                                 end
                                    2'b11: snext = `idle; //no ack or error
                              endcase
                              end
                              
                  `ack_nB: begin //at the entrance SCL is 1
                              SDAout = 1'b1;       
                              case({SDA,SCL})
                                    2'b00,2'b10: snext = `write_cycleA;
                                    2'b01: snext = state;
                                    2'b11: snext = `idle; //stop condition detected
                              endcase
                              end
                  `set_data_valid: begin
                              snext = `idle;
                              if(r_w) SetDataValid = 1'b1;
                              else SetDataValid = 1'b0;
                              //$display("%d",seq);
                              end
                   default: snext = `idle;
            endcase
      end
endmodule



 

