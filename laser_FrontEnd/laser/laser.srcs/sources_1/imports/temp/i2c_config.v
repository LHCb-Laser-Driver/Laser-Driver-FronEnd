module i2c_config ( 
input SCL,
input reset,
input refClk,
output SDAen,
output SDAo, 
input SDAi,
input [6:0] chip_id,
output [7:0] out0,
output [7:0] out1,
output [7:0] out2,
output [7:0] out3
 );
///////////////////////////////////////////////////////////////////////////////////////////
wire[7:0] data_r, data_w, mem_adr; 
wire we;
wire[2:0] statei2c;
wire clocki2c;  
wire resetb;
/////////////////////////////////////////////////////////////////////////////////////////////////
assign resetb = ~reset;
/////////////////////////////////////////////////////////////////////////////////////////////////
			
i2c_slave i2c_slave_inst( 
			.SCL(SCL), 
			.resetB(resetb), 
			.clk(refClk), 
			.wb_dat_i(data_r[7:0]), 
			.wb_we(we), 
			.wb_stb(),  
			.wb_cyc(), 
			.wb_ack(),
			.chip_id(chip_id), 
			.wb_dat_o(data_w[7:0]), 
			.SDAo(SDAo), 
			.SDAen(SDAen), 
			.wb_adr(mem_adr[7:0]),
			.SDAi(SDAi),
			.state(statei2c[2:0]),
     	    .clkg(clocki2c) 
			);

i2c_config_memory i2c_config_memory_inst (
			.data_r(data_r[7:0]), 
			.data_w(data_w[7:0]), 
			.gated_clk(clocki2c), 
			.write_mem_adr(mem_adr[7:0]), 
			.read_mem_adr(mem_adr[7:0]), 
			.rst(reset),
			.state(statei2c[2:0]),
			.we(we), 
			.default_values(32'h00000000),
			.out0(out0),
			.out1(out1),
			.out2(out2),
			.out3(out3)		
			);
endmodule