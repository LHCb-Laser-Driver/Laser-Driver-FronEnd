module i2c_config_memory (
output [7:0] data_r, 
input [7:0] data_w,
input gated_clk, 
input [7:0] write_mem_adr, 
input [7:0] read_mem_adr, 
input rst, 
input [2:0] state,
input we,
input [31:0] default_values,
output [7:0] out0,
output [7:0] out1,
output [7:0] out2,
output [7:0] out3
);

///////////////////////////////////////////////////////////////////////////////////////////  

/////////////////////////////////////////////////////////////////////////////////////////// 
 // Buses in the design

wire  [3:0]  load;
wire  [3:0]  loadClk;
///////////////////////////////////////////////////////////////////////////////////////////

i2c_memory i2c_memory_inst ( 
	.rst, 
	.data_w(data_w),
	.load(load),
	.mem_adr(read_mem_adr),
	.data_r(data_r[7:0]), 
	.state(state),
	.default_values(default_values),
	.out0(out0),
	.out1(out1),
	.out2(out2),
	.out3(out3),
	.clk(gated_clk)
    );

i2c_config_addr_dec i2c_config_addr_dec_inst (
     .we(we),
     .clk(gated_clk),
     .rst(rst),
     .mem_adr(write_mem_adr), 
     .load(load),
     .loadClk(loadClk)
     );

endmodule