module i2c_memory(
input rst,
input [7:0] data_w,
input [3:0] load,
input [7:0] mem_adr,
output [7:0] data_r,
input [2:0] state,
input [31:0] default_values,
output [7:0] out0,
output [7:0] out1,
output [7:0] out2,
output [7:0] out3,
input clk
);

///////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
parameter total = 4;

wire [(total*8)-1:0] mems;

genvar i;
generate
for (i=0; i<total ; i=i+1) begin: memgen
	i2c_config_cell mem_array(
					.rst(rst),
					//.clk(load_clk[i]), 
					.clk(clk),
					.in(data_w), 
					.load(load[i]), 
					.out(mems[(8*i)+7:8*i]), 
					.default_value(default_values[(8*i)+7:8*i]) 
					);
end
endgenerate


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//assign #1 data_r = mems[(8*mem_adr)+:8];
assign #1 data_r = (mem_adr < total) ? mems[(8*mem_adr)+:8] : 8'h00;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// -------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// do mapping

assign out0	=   mems [7	  :0	   ];	// 0
assign out1	=   mems [15	  :8	   ];
assign out2	=   mems [23	  :16	   ];
assign out3	=   mems [31	  :24	   ];

endmodule