///////////////////////////////////////////////////
//
// i2c_slave_iostate.v
// Created on Jun  3  2008 
// by Sandro Bonacini
// CERN PH/ESE/ME
//
///////////////////////////////////////////////////
//22/3/2011 added START_late wire and assign
///////////////////////////////////////////////////
// adapted by Ken Wyllie for GBTX
// addressing modified
// wishbone interface minmised
`timescale 1ps/1ps

module i2c_slave_iostate ( SCL, resetB, clk, wb_dat_i, wb_we,
				chip_id, wb_dat_o, SDAo,
				SDAen, wb_adr, SDAi, wb_dat_o_voted, wb_adr_voted,   state, i,
				watchdog, buffer, state_voted, i_voted, watchdog_voted,
				buffer_voted, en_clk, selected, readcycle, en_clk_voted,
				selected_voted, readcycle_voted, wb_we_voted, SDAen_voted,
				SDAo_voted,
				clkg );

    	input	SCL;
	input	SDAi;
    	input  	 resetB;
    	input	clk;
	input [7:0] wb_dat_i;     // databus input
	input [6:0] chip_id;
	output        wb_we;      // write enable input
	output [7:0] wb_dat_o;     // databus output
	output SDAo;
	output SDAen;
	output [15:0] wb_adr;

	input [7:0] wb_dat_o_voted;     // databus wire
	input [15:0] wb_adr_voted;	
	
    	output [2:0]  state;
	output [3:0] i;
	output [8:0] watchdog;
	output [7:0] buffer;
	output en_clk;
	output selected;
	output readcycle;
    	input [2:0]  state_voted;
	input [3:0] i_voted;
	input [8:0] watchdog_voted;
	input [7:0] buffer_voted;
	input en_clk_voted;
	input selected_voted;
	input readcycle_voted;
	input wb_we_voted;
	input SDAen_voted;
	input SDAo_voted;

	output clkg;
	
/////////////////////////////////////////////////////////////////////////////////////	
	wire STARTorSTART_late;
	wire START_late;
	
	wire STOPorstate_voted0;

	i2c_synchronizer synch1 ( 
		.S_SCL(S_SCL),
		.clk40(clk),
		.SCL(SCL),
		.SDA(SDAi),
		.START(START),
		.STOP(STOP),
		.resetB(resetB),
		.S_SDA(S_SDA)
	);

// KHW addition 22/3/2011
	assign STARTorSTART_late =  START | START_late;
// end of addition

	i2c_control slave1  (
		.SDAi(S_SDA),
		.SCL(S_SCL),
		.START(STARTorSTART_late),
		.STOP(STOP),
		.resetB(resetB),
		.clk(clkg),
		.wb_adr(wb_adr),
		.wb_dat_i(wb_dat_i),
		.wb_dat_o(wb_dat_o),
		.SDAo(SDAo),
		.SDAen(SDAen),
		.state(state),
		.wb_we(wb_we),
		.i(i),
		.buffer(buffer),
		.i_voted(i_voted),
		.buffer_voted(buffer_voted),
		.wb_dat_o_voted(wb_dat_o_voted),
		.SDAo_voted(SDAo_voted),
		.SDAen_voted(SDAen_voted),
		.state_voted(state_voted),
		.wb_adr_voted(wb_adr_voted),
		.wb_we_voted(wb_we_voted),
		.readcycle(readcycle),
		.selected(selected),
		.readcycle_voted(readcycle_voted),
		.selected_voted(selected_voted),
		.watchdog(watchdog),
		.watchdog_voted(watchdog_voted),
		.chip_id(chip_id)
	);

// KHW addition 22/3/2011
	assign STOPorstate_voted0 =  STOP | (state_voted==0);
// end of addition
	
	i2c_clock_gating i2c_clock_gating1 (
		.START(START),
		.START_late(START_late),
		.clk(clk),
		.clkg(clkg),
		.en_clk(en_clk),
		.en_clk_voted(en_clk_voted),
		.STOP(STOPorstate_voted0),
		.resetB(resetB)
	);


endmodule