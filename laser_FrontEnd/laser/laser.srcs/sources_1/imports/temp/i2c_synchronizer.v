///////////////////////////////////////////////////
//
// i2c_synchronizer.v
// Created on Jun  3  2008 
// by Sandro Bonacini
// CERN PH/ESE/ME
//
///////////////////////////////////////////////////
`timescale 1ps/1ps
module i2c_synchronizer ( S_SCL, clk40, SCL, SDA, START, STOP, resetB, S_SDA  );

    input  clk40, SCL, SDA, resetB;
    
    output S_SCL, START, STOP, S_SDA;
    
	reg S_SCL, S_SDA, SCL_late, SDA_late, sda_fall, sda_rise;
	reg [1:0] sda_fall_late, sda_rise_late;

	assign START = (sda_fall_late == 'b01);
	assign STOP = (sda_rise_late == 'b01);
	
	always @(posedge clk40 or negedge resetB) begin
		if (~resetB) begin
			S_SCL <=#1 0;
			S_SDA <=#1 1;
			SCL_late<=#1 0;
			SDA_late<=#1 1;
			sda_rise_late <=#1 0;
			sda_fall_late <=#1 0;
		end
		else begin
			{S_SCL, SCL_late} <=#1 {SCL_late, SCL};
			{S_SDA, SDA_late} <=#1 {SDA_late, SDA};

			sda_fall_late <=#1 {sda_fall_late[0], sda_fall};
			sda_rise_late <=#1 {sda_rise_late[0], sda_rise};
		end
	end
	
	always @(posedge SDA or negedge resetB) begin
		if (~resetB) sda_rise <=#1 0;
		else sda_rise <=#1 SCL;
	end

	always @(negedge SDA or negedge resetB) begin
		if (~resetB) sda_fall <=#1 0;
		else sda_fall <=#1 SCL;
	end

endmodule