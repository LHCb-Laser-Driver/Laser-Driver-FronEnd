///////////////////////////////////////////////////
//
// i2c_slave_noTMR.v
// Created on Jun  3  2008 
// by Sandro Bonacini
// CERN PH/ESE/ME
//
///////////////////////////////////////////////////

module i2c_slave ( SCL, resetB, clk, wb_dat_i, wb_we, wb_stb,  wb_cyc, wb_ack,
			chip_id, wb_dat_o, SDAo, SDAen, wb_adr,
			SDAi, state, clkg  );

	parameter [3:0] CHIP_ID_WIDTH = 3;

    input     	    SCL;
	input			SDAi;
    input  	    resetB;
    input   	    clk;
	input [7:0] wb_dat_i;     // databus input
	output        wb_we;      // write enable input
	output        wb_stb;     // stobe/core select signal
	output        wb_cyc;     // valid bus cycle input
	input       wb_ack;     // bus cycle acknowledge output
	input [6:0] chip_id;
	output [7:0] wb_dat_o;     // databus output
	output SDAo, SDAen;
	output [7:0] wb_adr;
	output [2:0] state;
	output clkg;
	
	
    wire  	    SDAo;
    wire  	    SDAen;
	wire [3:0] i;
	wire [8:0] watchdog;
	wire [7:0] buffer;
	wire [7:0] wb_adr_unused_bits;

	i2c_slave_iostate i2c_slave_iostate_inst  (
		.SCL(SCL), 
		.resetB(resetB),
		.clk(clk), 
		.wb_dat_i(wb_dat_i), 
		.wb_we(wb_we),
		.chip_id(chip_id), 
		.wb_dat_o(wb_dat_o), 
		.SDAo(SDAo),
		.SDAen(SDAen), 
		.wb_adr({wb_adr_unused_bits,wb_adr}),
		.SDAi(SDAi), 
		.wb_dat_o_voted(wb_dat_o), 
		.wb_adr_voted({wb_adr_unused_bits,wb_adr}),   
		.state(state), 
		.i(i),
		.watchdog(watchdog), 
		.buffer(buffer), 
		.state_voted(state), 
		.i_voted(i), 
		.watchdog_voted(watchdog),
		.buffer_voted(buffer), 
		.en_clk(en_clk), 
		.selected(selected), 
		.readcycle(readcycle), 
		.en_clk_voted(en_clk),
		.selected_voted(selected), 
		.readcycle_voted(readcycle), 
		.wb_we_voted(wb_we), 
		.SDAen_voted(SDAen),
		.SDAo_voted(SDAo),
		.clkg(clkg)
		);
endmodule
