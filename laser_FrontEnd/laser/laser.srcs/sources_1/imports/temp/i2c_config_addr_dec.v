module i2c_config_addr_dec (
				input we, 
				input clk,
				input rst,
				input[7:0] mem_adr,
				output[3:0] load,
				output[3:0] loadClk
				);

// -------------------------------------------------------------------------
	//input configSelect;	// 0 = sc bits from link, 1 = I2C
// -------------------------------------------------------------------------	
	parameter total = 4;
	
	reg [3:0] load_del;
	wire [3:0] clockGate;
	reg [3:0] loadClki2c;
	reg [3:0] loadClksc;
	
	reg [3:0] load_latched;
//////////////////////////////////////////////////////////////////////
	// logic for writing to memory
	
	assign #1 load = (we & (mem_adr<total))? 2 ** mem_adr : 4'b0;

endmodule