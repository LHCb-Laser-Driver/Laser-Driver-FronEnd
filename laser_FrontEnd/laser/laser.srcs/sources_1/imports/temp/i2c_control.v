///////////////////////////////////////////////////
//
// i2c_control.v
// Created on Jun  3  2008 
// by Sandro Bonacini
// CERN PH/ESE/ME
//
///////////////////////////////////////////////////
// adapted by Ken Wyllie for GBTX
// addressing modified
// wishbone interface minmised
//
// 13/7/2011
// Expand internal memory address space to 2 bytes, add new state ADDR3
//
// Sept 2017: modified for LHCb RICH laser pulser
// ADDR3 not used (only 8-bit address)

`timescale 1ps/1ps

////////////////////////////////////////////////////////////////////////////////////////////
module i2c_control (SDAi, SCL, START, STOP, resetB, clk, wb_adr,
			wb_we, wb_dat_i, wb_dat_o, SDAo, SDAen, state, i, buffer, i_voted,
			buffer_voted, wb_dat_o_voted, SDAo_voted, SDAen_voted, state_voted,
			wb_adr_voted, wb_we_voted, readcycle, selected,
			readcycle_voted, selected_voted, watchdog, watchdog_voted, chip_id
			);

	parameter [8:0] WATCHDOG_PRESET = 511;

    	input     	    SDAi;
    	input     	    SCL;
    	input   	    START;
    	input   	    STOP;
    	input   	    resetB;
    	input   	    clk;
	
	output[15:0] wb_adr; 	// address of config register to write to   
	input [7:0] wb_dat_i;	// databus input to read from config regs
	output[7:0] wb_dat_o;	// databus output to write to config regs
	output        wb_we;	// write enable output to config registers
	
	input [6:0] chip_id;
	
    	input [2:0] state_voted; 	
	input [3:0] i_voted;
	input [7:0] buffer_voted;
	input [7:0] wb_dat_o_voted;
	input [15:0] wb_adr_voted;
	input SDAo_voted, SDAen_voted, wb_we_voted;
	input readcycle_voted, selected_voted;
	input [8:0]	watchdog_voted;
      
    	output  [2:0]  state;
    	output  	    SDAo;
    	output  [7:0]   buffer;
    	output  	    SDAen;
	output [3:0] i;
	output readcycle, selected;
	output [8:0] watchdog;
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
    	reg [7:0]   wb_dat_o;
	reg [15:0] wb_adr;
	reg SDAo, SDAen, wb_we;
	reg [8:0] watchdog;

	parameter [2:0]  
		IDLE = 	3'd0,
		ADDR = 	3'd1,
		ADDR2 = 3'd2,
		ADDR3 = 3'd3,
		RXTX = 	3'd4;
	
	
	reg [3:0] i;
	reg [2:0] state;
	reg SCL_late;
	reg [7:0] buffer;
	reg readcycle;
	reg selected;
	
	
    always  @(posedge clk or negedge resetB) begin
		if (~resetB) begin
	    	i 	    	<=#1 0;
	    	buffer 	<=#1 0;
	    	wb_dat_o    	<=#1 0;
	    	SDAo	 	<=#1 1'b1;
	    	SDAen   	<=#1 1'b0;
	    	state 	<=#1 IDLE;
			SCL_late <=#1 0;
			wb_adr <=#1 0;
			wb_we <=#1 0;
			selected <=#1 0;
			readcycle <=#1 0;
			watchdog <=#1 WATCHDOG_PRESET;
		end
		else begin
			i <=#1 i_voted;
			state <=#1 state_voted;
			SCL_late <=#1 SCL;
			wb_dat_o <=#1 wb_dat_o_voted;
			SDAo <=#1 SDAo_voted;
			SDAen <=#1 SDAen_voted;
			buffer <=#1 buffer_voted;
			wb_adr <=#1 wb_adr_voted;
			wb_we <=#1 wb_we_voted;
			readcycle <=#1 readcycle_voted;
			selected <=#1 selected_voted;

			case (state_voted) //synopsys full_case parallel_case
				IDLE: begin
					SDAen <=#1 0;
					wb_we <=#1 0;
				end
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
				ADDR: begin			// get slave address and RW_ bit
					wb_we <=#1 0;
					if ({SCL_late,SCL}=='b01) i<=#1 i_voted + 1;

					if (i_voted<8) begin
						SDAen <=#1 0;
						if ({SCL_late,SCL}=='b01) begin
							buffer <=#1 {buffer_voted[6:0], SDAi};	
						end
					end
					
					if (i_voted==8) begin
						if ({SCL_late,SCL}=='b10) begin
							if (	(buffer_voted[7:1] == chip_id) |
								(buffer_voted[7:1] == 7'b000_0000))	// general call address
							begin
								SDAen<=#1 1; 		// ACK, send 0 to SDA
								SDAo<=#1 0;	// 
								readcycle<=#1 buffer_voted[0];
								if (buffer_voted[0]) begin
									wb_we <=#1 0;
								end
							end
							else state <=#1 IDLE;
						end
					end
					
					if (i_voted==9) begin
						if (buffer_voted[0]) begin // READ
							i <=#1 0;
							state <=#1 RXTX;
							buffer <=#1 wb_dat_i;
						end
						else begin // WRITE
							if ({SCL_late,SCL}=='b10) begin
								i<=#1 0;
								SDAen <=#1 0;
								state <=#1 ADDR2;
							end
						end
					end
				end
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
				ADDR2: begin		// get first byte of internal memory address
					wb_we <=#1 0;
					if ({SCL_late,SCL}=='b01) i<=#1 i_voted + 1;

					if (i_voted<8) begin
						SDAen <=#1 0;
						if ({SCL_late,SCL}=='b01) begin
							buffer <=#1 {buffer_voted[6:0], SDAi};	
						end
					end
					
					if (i_voted==8) begin
						if ({SCL_late,SCL}=='b10) begin
							SDAen<=#1 1; 		// ACK, send 0 to SDA
							SDAo<=#1 0;	// 
							wb_adr[7:0]<=#1 buffer_voted[7:0];
						end
					end
					
					if (i_voted==9) begin
						if ({SCL_late,SCL}=='b10) begin
							i<=#1 0;
							SDAen <=#1 0;
							state <=#1 RXTX; //ADDR3;
						end
					end
				end
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
				ADDR3: begin		// get second byte of internal memory address
					wb_we <=#1 0;
					if ({SCL_late,SCL}=='b01) i<=#1 i_voted + 1;

					if (i_voted<8) begin
						SDAen <=#1 0;
						if ({SCL_late,SCL}=='b01) begin
							buffer <=#1 {buffer_voted[6:0], SDAi};	
						end
					end
					
					if (i_voted==8) begin
						if ({SCL_late,SCL}=='b10) begin
							SDAen<=#1 1; 		// ACK, send 0 to SDA
							SDAo<=#1 0;	// 
							wb_adr[15:8]<=#1 buffer_voted[7:0];
						end
					end
					
					if (i_voted==9) begin
						if ({SCL_late,SCL}=='b10) begin
							i<=#1 0;
							SDAen <=#1 0;
							state <=#1 RXTX;
						end
					end
				end
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
				RXTX: if (!readcycle) begin // RX
					if ({SCL_late,SCL}=='b01) i<=#1 i_voted + 1;

					if (i_voted<8) begin
						SDAo <=#1 1;
						SDAen <=#1 0;
						if ({SCL_late,SCL}=='b01) begin
							buffer <=#1 {buffer_voted[6:0], SDAi};	
						end
					end

					if (i_voted==8) begin
						if ({SCL_late,SCL}=='b10) begin
							SDAen<=#1 1; 		// ACK, send 0 to SDA
							SDAo<=#1 0;	// 
							wb_dat_o<=#1 buffer_voted;
							wb_we <=#1 1;	// 
						end
					end

					if (i_voted==9) begin
						if ({SCL_late,SCL}=='b10) begin
							SDAen<=#1 0;
							i<=#1 0;
							wb_we <=#1 0;
							wb_adr<=#1 wb_adr_voted + 1; // increment internal address for block
						end
					end

				end
 				else begin // TX
					if ({SCL_late,SCL}=='b10) i<=#1 i_voted + 1;

					if (i_voted<8) begin
						SDAen <=#1 1;
						if ({SCL_late,SCL}=='b10) begin
							SDAo <=#1 buffer_voted[7];
							buffer <=#1 buffer_voted << 1;
						end
					end

					if (i_voted==8) begin
						if ({SCL_late,SCL}=='b10) begin
							SDAen<=#1 0; // ACK should come from master receiver
							wb_adr<=#1 wb_adr_voted + 1; // increment internal address for block
						end
					end

					if (i_voted==9) begin
						SDAen<=#1 0;
						if ({SCL_late,SCL}=='b01) begin
							if (~SDAi) begin
								i<=#1 0;
								buffer <=#1 wb_dat_i;
							end
							else state <=#1 IDLE;
						end
					end

				end

			endcase
			
			//if (STOP | (watchdog_voted==0)) begin
			if (STOP) begin
				selected <=#1 0;
				state<=#1 IDLE;
			end

			if (START) begin
				state <=#1 ADDR;
				SDAen <=#1 0;
				i<=#1 0;
			end

			if (SCL==1) begin  // Avoid changes when SCL is high
				SDAen <=#1 SDAen_voted;
				SDAo <=#1 SDAo_voted;
			end
			
			
			if (({SCL_late,SCL}=='b10) | (state_voted==IDLE)) watchdog <=#1 WATCHDOG_PRESET;
			else watchdog <=#1 watchdog_voted - 1;
		end
	end

endmodule