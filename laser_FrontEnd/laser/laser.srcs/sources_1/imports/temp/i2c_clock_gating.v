//////////////////////////////////////////////////
//
// i2c_clock_gating_iostate.v
// Created on Jun  3  2008 
// by Sandro Bonacini
// CERN PH/ESE/ME
//
///////////////////////////////////////////////////
`timescale 1ps/1ps
module i2c_clock_gating (START, START_late, clk, clkg, en_clk, en_clk_voted,
						STOP, resetB);
	
	output START_late, clkg, en_clk;
	input START, STOP, en_clk_voted, clk, resetB;

	reg START_late;
	reg en_clk, en_clk_latched;
	//always @(clk) begin
	//	if (~clk) en_clk_latched = en_clk_voted | START;
	//end
	
	always @(negedge clk) begin
		en_clk_latched <=  #1 en_clk_voted | START;
	end
	
	assign  clkg = clk & en_clk_latched;
		
	always @(posedge clk or negedge resetB) begin
		if (~resetB) begin
			en_clk <=#1 0;
			START_late <=#1 0;
		end
		else begin
			en_clk <=#1 en_clk_voted;
			if (START) en_clk <=#1 1;
			else begin
				if (STOP) en_clk <=#1 0;
			end
			
			//if ((state==0) & (!START)) en_clk <=#1 0;
			START_late <=#1 START;
		end
	end

endmodule