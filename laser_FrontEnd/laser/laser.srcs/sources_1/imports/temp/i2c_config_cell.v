module i2c_config_cell (
						input rst, 
						input clk, 
						input[7:0] in, 
						input load, 
						output[7:0] out, 
						input[7:0] default_value
						);
			
// -------------------------------------------------------------------------
	// declare memory
	reg [7:0] mem;

// -------------------------------------------------------------------------
	// write to memory 	
	always@(posedge clk or posedge rst)
		if (rst)
			mem <= default_value;
		else if(load)
			mem <= in;
	    else
	        mem <= mem;
	
	assign #1 out = mem;
	
endmodule