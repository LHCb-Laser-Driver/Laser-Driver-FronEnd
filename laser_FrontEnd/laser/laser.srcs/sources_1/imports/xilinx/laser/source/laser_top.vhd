----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Ken
-- 
-- Create Date: 05.07.2017 16:49:41
-- Design Name: 
-- Module Name: laser_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

--! Include the work library
library work;
--! Use SCA Package to define specific types (vector arrays)
use work.LCD_PKG.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_banks_user_setup.all;

entity laser_top is
Port ( 

      
      --===============--     
      -- General reset --     
      --===============--     

      CPU_RESET                                      : in  std_logic;     -- checked
      
      --===============--
      -- Clocks scheme --
      --===============-- 
      
     -- System clock:
       ----------------
       SYSCLK_P                                     : in  std_logic;
       SYSCLK_N                                     : in  std_logic;   

      -- Fabric clock:
      ----------------     

      USER_CLOCK_P                                   : in  std_logic;     -- checked
      USER_CLOCK_N                                   : in  std_logic;     -- checked   
      
      -- MGT(GTX) reference clock:
      ----------------------------
      
      -- Comment: * The MGT reference clock MUST be provided by an external clock generator.
      --
      --          * The MGT reference clock frequency must be 120MHz for the latency-optimized GBT Bank.      
      
      SMA_MGT_REFCLK_P                               : in  std_logic;   -- checked
      SMA_MGT_REFCLK_N                               : in  std_logic;   -- checked
      
      --==========--
      -- MGT(GTX) --
      --==========--                   
      
      -- Serial lanes:
      ----------------
      
      SFP_TX_P                                       : out std_logic;   -- checked
      SFP_TX_N                                       : out std_logic;   -- checked
      SFP_RX_P                                       : in  std_logic;   -- checked
      SFP_RX_N                                       : in  std_logic;   -- checked                 
      
      -- SFP control:
      ---------------
      
      SFP_TX_DISABLE                                 : out std_logic;   -- checked
      
      --===============--
      -- LCD           --
      --===============--
      LCD_DB4_LS          : out std_logic;
      LCD_DB5_LS          : out std_logic;
      LCD_DB6_LS          : out std_logic;
      LCD_DB7_LS          : out std_logic;
      LCD_RW_LS           : out std_logic;
      LCD_RS_LS           : out std_logic;
      LCD_E_LS            : out std_logic;
      --===============--      
      -- On-board LEDs --      
      --===============--

      GPIO_LED_0_LS                                  : out std_logic;   -- checked
      GPIO_LED_1_LS                                  : out std_logic;   -- checked
      GPIO_LED_2_LS                                  : out std_logic;   -- checked
      GPIO_LED_3_LS                                  : out std_logic;   -- checked
      GPIO_LED_4_LS                                  : out std_logic;   -- checked
      GPIO_LED_5_LS                                  : out std_logic;   -- checked
      GPIO_LED_6_LS                                  : out std_logic;   -- checked
      GPIO_LED_7_LS                                  : out std_logic;    -- checked     
      
     -- User Switches
      GPIO_DIP_SW0                                   : in std_logic;
      GPIO_DIP_SW1                                   : in std_logic;
      GPIO_DIP_SW2                                   : in std_logic;
      GPIO_DIP_SW3                                   : in std_logic;
    -- reset switches: 
      GPIO_SW_C                                      : in std_logic;       
      GPIO_SW_E                                      : in std_logic;
      GPIO_SW_N                                      : in std_logic;
      --====================--
      -- Signals forwarding --
      --====================--
      
      -- SMA output:
      --------------
            
      USER_SMA_GPIO_P                                : out std_logic;     -- checked 
      USER_SMA_GPIO_N                                : out std_logic;     -- checked  
      USER_SMA_CLOCK_N                               : in std_logic;
      USER_SMA_CLOCK_P                               : inout std_logic;
      -- Pattern match flags:
      -----------------------
      
      FMC_LPC_LA00_CC_P                              : out std_logic;       -- checked      
      FMC_LPC_LA01_CC_P                              : out std_logic;   -- checked
      
      -- Clocks forwarding:
      ---------------------  
      
      -- Comment: * FMC_HPC_LA02_P and FMC_HPC_LA03_P are used for forwarding TX_FRAMECLK and TX_WORDCLK respectively.
      --      
      --          * FMC_HPC_LA04_P and FMC_HPC_LA05_P are used for forwarding RX_FRAMECLK and RX_WORDCLK respectively.
      
      FMC_LPC_LA02_P                                 : out std_logic;   -- checked
      FMC_LPC_LA03_P                                 : out std_logic;    -- checked
      -----------------------------------------------
      FMC_LPC_LA04_P                                 : out std_logic;    -- checked
      FMC_LPC_LA05_P                                 : out std_logic;    -- checked
      FMC_LPC_LA06_P                                 : out std_logic;    -- checked
      FMC_LPC_LA07_P                                 : out std_logic;    -- checked
      FMC_LPC_LA08_P                                 : out std_logic;    -- checked
      FMC_LPC_LA09_P                                 : out std_logic;    -- checked
      FMC_LPC_LA10_P                                 : out std_logic;    -- checked
      FMC_LPC_LA11_P                                 : out std_logic;    -- checked
      FMC_LPC_LA12_P                                 : out std_logic;    -- checked
      FMC_LPC_LA13_P                                 : out std_logic;    -- checked
      FMC_LPC_LA14_P                                 : out std_logic;    -- checked
      FMC_LPC_LA15_P                                 : out std_logic;    -- checked
		
		--elinks monitoring on HPC
		-----------------------
		--FMC_HPC_LA_p												: inout	std_logic_vector(0 to 23);
		--FMC_HPC_LA_n												: inout	std_logic_vector(0 to 23)
		FMC_HPC_LA00_CC_N                             : inout std_logic;
		FMC_HPC_LA00_CC_P                             : inout std_logic;
		FMC_HPC_LA01_CC_N                             : inout std_logic;
		FMC_HPC_LA01_CC_P                             : inout std_logic;
		
		FMC_HPC_LA02_N                                : inout std_logic;
		FMC_HPC_LA02_P                                : inout std_logic;
		FMC_HPC_LA03_N                                : inout std_logic;
        FMC_HPC_LA03_P                                : inout std_logic;
        --FMC_HPC_LA04_N                                : inout std_logic;
        --FMC_HPC_LA04_P                                : inout std_logic;
        FMC_HPC_LA05_N                                : inout std_logic;
        FMC_HPC_LA05_P                                : inout std_logic;
        FMC_HPC_LA06_N                                : inout std_logic;
        FMC_HPC_LA06_P                                : inout std_logic;
        FMC_HPC_LA07_N                                : inout std_logic;
        FMC_HPC_LA07_P                                : inout std_logic;
        FMC_HPC_LA08_N                                : inout std_logic;
        FMC_HPC_LA08_P                                : inout std_logic;
        --FMC_HPC_LA09_N                                : inout std_logic;
        --FMC_HPC_LA09_P                                : inout std_logic;
        FMC_HPC_LA10_N                                : inout std_logic;
        FMC_HPC_LA10_P                                : inout std_logic;
        FMC_HPC_LA11_N                                : inout std_logic;
        FMC_HPC_LA11_P                                : inout std_logic;
        FMC_HPC_LA12_N                                : inout std_logic;
        FMC_HPC_LA12_P                                : inout std_logic;
        FMC_HPC_LA13_N                                : inout std_logic;
        FMC_HPC_LA13_P                                : inout std_logic;
        --FMC_HPC_LA14_N                                : inout std_logic;
        --FMC_HPC_LA14_P                                : inout std_logic;
        FMC_HPC_LA15_N                                : inout std_logic;
        FMC_HPC_LA15_P                                : inout std_logic;
        FMC_HPC_LA16_N                                : inout std_logic;
        FMC_HPC_LA16_P                                : inout std_logic;
        FMC_HPC_LA17_CC_N                                : inout std_logic;
        FMC_HPC_LA17_CC_P                                : inout std_logic;
        FMC_HPC_LA18_CC_N                                : inout std_logic;
        FMC_HPC_LA18_CC_P                                : inout std_logic;
        --FMC_HPC_LA19_N                                : inout std_logic;
        --FMC_HPC_LA19_P                                : inout std_logic;
        FMC_HPC_LA20_N                                : inout std_logic;
        FMC_HPC_LA20_P                                : inout std_logic;
        FMC_HPC_LA21_N                                : inout std_logic;
        FMC_HPC_LA21_P                                : inout std_logic;
        FMC_HPC_LA22_N                                : inout std_logic;
        FMC_HPC_LA22_P                                : inout std_logic;
        FMC_HPC_LA23_N                                : inout std_logic;
        FMC_HPC_LA23_P                                : inout std_logic;
        
-- following pair are connected to SMAs on the FMC mezzanine
-- these MRCC pins: use them for the 40MHz from the GBTX desclocks        
        FMC_HPC_CLK0_M2C_P                              : in std_logic;
        FMC_HPC_CLK0_M2C_N                              : in std_logic
);
end laser_top;

architecture structural of laser_top is

   --================================ Signal Declarations ================================--          

--===============--     
-- General reset --     
--===============--     

signal reset_from_genRst                          : std_logic; 
signal reset_from_genRst_n                        : std_logic;    

--===============--
-- Clocks scheme -- 
--===============--   

-- Fabric clock:
----------------

signal fabricClk_from_userClockIbufgds            : std_logic;     

-- MGT(GTX) reference clock:     
----------------------------     

signal mgtRefClk_from_smaMgtRefClkIbufdsGtxe2     : std_logic;   

 -- Frame clock:
 ---------------
 signal txFrameClk_from_txPll                     : std_logic;
 
--================--
-- Clock component--
--================--
-- Vivado synthesis tool does not support mixed-language
-- Solution: http://www.xilinx.com/support/answers/47454.html
COMPONENT tx_pll PORT(
   clk_in1: in std_logic;
   RESET: in std_logic;
   CLK_OUT1: out std_logic;
   LOCKED: out std_logic
);
END COMPONENT;

COMPONENT elink_des PORT(
    dataIn320 : in std_logic;
    clk320 : in std_logic;
    dataOut40 : out std_logic_vector(7 downto 0);
    clk40 : in std_logic;
    selectBits : in std_logic_vector(1 downto 0);
    codeOK : out std_logic;
    serialOut : out std_logic
    );
END COMPONENT;    

COMPONENT test_not PORT(
    dataIn320 : in std_logic;
    Y : out std_logic;
    clk: in std_logic
    );
END COMPONENT;

COMPONENT phase_shift_PLL PORT(
   --Clock in ports
    clk_in1 : in std_logic;      
    -- Clock out ports
    clk_out1 : out std_logic;    
    -- Dynamic phase shift ports
    psclk : in std_logic; 
    psen : in std_logic;
    psincdec : in std_logic;    
    psdone : out std_logic;     
    -- Status and control signals
    reset : in std_logic;
    locked : out std_logic
    ); 
END COMPONENT;

COMPONENT phase_shift_control PORT(
    reset_in : in std_logic;
    sysclk : in std_logic;
    psen : out std_logic;
    psdone  : in std_logic;
    max_count : in std_logic_vector(10 downto 0);
    enable_pulse : out std_logic;
    select_neg : out std_logic;
    resetPLL : out std_logic;
    PLLLocked : in std_logic
    );
END COMPONENT;

COMPONENT sca_driver PORT(
    reset : in std_logic;
    clock : in std_logic;
    start_in : in std_logic;
    startConnect  : out std_logic;
    startTransaction  : out std_logic;
    tx_addr  : out std_logic_vector(7 downto 0); 
    tx_trid  : out std_logic_vector(7 downto 0); 
    tx_ch   : out std_logic_vector(7 downto 0);
    tx_cmd  : out std_logic_vector(7 downto 0);
    tx_data  : out std_logic_vector(31 downto 0);  
    rx_trid  : in std_logic_vector(7 downto 0);
    state  : out std_logic_vector(7 downto 0);
    byte0 : in std_logic_vector(7 downto 0);
    byte1 : in std_logic_vector(7 downto 0);
    byte2 : in std_logic_vector(7 downto 0);
    byte3 : in std_logic_vector(7 downto 0)
);
END COMPONENT;

COMPONENT I2C_slave_7b10bAdr PORT (
      clk : in std_logic;
      resetb : in std_logic; --active low
      SCL  : in std_logic;
      SDA  : in std_logic;
      SDAout  : out std_logic;
      I2CDataRec : out std_logic_vector(31 downto 0);
      I2CDataSend : in std_logic_vector(31 downto 0);
      SetDataValid : out std_logic
      ); 
END COMPONENT;

COMPONENT  i2c_config PORT ( 
    SCL : in std_logic;
    reset : in std_logic;
    refClk : in std_logic;
    SDAen  : out std_logic;
    SDAo  : out std_logic; 
    SDAi : in std_logic;
    chip_id : in std_logic_vector(6 downto 0);
    out0 : out std_logic_vector(7 downto 0);
    out1 : out std_logic_vector(7 downto 0);
    out2 : out std_logic_vector(7 downto 0);
    out3 : out std_logic_vector(7 downto 0)
 );
END COMPONENT;
--=========================--
-- GBT Bank example design --
--=========================--

-- Control:
-----------

signal generalReset_from_user                     : std_logic;      
signal manualResetTx_from_user                    : std_logic; 
signal manualResetRx_from_user                    : std_logic; 
signal clkMuxSel_from_user                        : std_logic;       
signal testPatterSel_from_user                    : std_logic_vector(1 downto 0); 
signal loopBack_from_user                         : std_logic_vector(2 downto 0); 
signal resetDataErrorSeenFlag_from_user           : std_logic; 
signal resetGbtRxReadyLostFlag_from_user          : std_logic; 
signal txIsDataSel_from_user                      : std_logic;   
--------------------------------------------------      
signal latOptGbtBankTx_from_gbtExmplDsgn          : std_logic;
signal latOptGbtBankRx_from_gbtExmplDsgn          : std_logic;
signal txFrameClkPllLocked_from_gbtExmplDsgn      : std_logic;
signal mgtReady_from_gbtExmplDsgn                 : std_logic; 
signal rxBitSlipNbr_from_gbtExmplDsgn             : std_logic_vector(GBTRX_BITSLIP_NBR_MSB downto 0);
signal rxWordClkReady_from_gbtExmplDsgn           : std_logic; 
signal rxFrameClkReady_from_gbtExmplDsgn          : std_logic; 
signal gbtRxReady_from_gbtExmplDsgn               : std_logic;    
signal rxIsData_from_gbtExmplDsgn                 : std_logic;        
signal gbtRxReadyLostFlag_from_gbtExmplDsgn       : std_logic; 
signal rxDataErrorSeen_from_gbtExmplDsgn          : std_logic; 
signal rxExtrDataWidebusErSeen_from_gbtExmplDsgn  : std_logic; 

-- Data:
--------

signal txData_from_gbtExmplDsgn                   : std_logic_vector(83 downto 0);
signal rxData_from_gbtExmplDsgn                   : std_logic_vector(83 downto 0);
--------------------------------------------------      
signal txExtraDataWidebus_from_gbtExmplDsgn       : std_logic_vector(31 downto 0);
signal rxExtraDataWidebus_from_gbtExmplDsgn       : std_logic_vector(31 downto 0);
      
--===========--
-- Chipscope --
--===========--

signal vioControl_from_icon                       : std_logic_vector(35 downto 0); 
signal txIlaControl_from_icon                     : std_logic_vector(35 downto 0); 
signal rxIlaControl_from_icon                     : std_logic_vector(35 downto 0); 
--------------------------------------------------
signal sync_from_vio                              : std_logic_vector(11 downto 0);
signal async_to_vio                               : std_logic_vector(17 downto 0);
signal PLL_clk_out, clk_320 					  : std_logic;
signal clk_320_LOCKED 							  : std_logic_vector(0 downto 0);
--=====================--
-- Latency measurement --
--=====================--

signal txFrameClk_from_gbtExmplDsgn               : std_logic;
signal txWordClk_from_gbtExmplDsgn                : std_logic;
signal rxFrameClk_from_gbtExmplDsgn               : std_logic;
signal rxWordClk_from_gbtExmplDsgn                : std_logic;
--------------------------------------------------                                    
signal txMatchFlag_from_gbtExmplDsgn              : std_logic;
signal rxMatchFlag_from_gbtExmplDsgn              : std_logic;

signal errCnter                                   : std_logic_vector(63 downto 0);
signal wordCnter                                  : std_logic_vector(63 downto 0);

--=====================--
-- elink signals --
--=====================--
signal elink_clk, elink_dout, elink_din, elink_dio : std_logic_vector (5 downto 1);
   
--================--

signal sysclk                                       : std_logic;
signal test_PLL_clk_out, test_PLL_LOCKED 			: std_logic;
signal dataOut40_1, dataOut40_2                     : std_logic_vector(7 downto 0);
signal selectBits_1, selectBits_2                   : std_logic_vector(1 downto 0);
signal clk40                                        : std_logic;

signal dataToTX                                     : std_logic_vector(83 downto 0);
signal frame0, frame1                               : std_logic_vector(7 downto 0);
signal frame2                                       : std_logic_vector(63 downto 0);
signal ic_frametoGBTX, ec_frametoGBTX               : std_logic_vector(1 downto 0);
signal reset_gbtsc_top                              : std_logic;
signal startConnect, startTransaction               : std_logic;
signal rx_trid                                      : std_logic_vector(7 downto 0);
signal sca_driver_state                             : std_logic_vector(7 downto 0);
signal rx_received                                  : std_logic;
signal byte0,byte1,byte2,byte3                      : std_logic_vector(7 downto 0);

signal I2CDataRec, I2CDataSend                      : std_logic_vector(31 downto 0);
signal SDA_in, SDA_out, SCL_in, SDA_en, SDA_en_b     : std_logic;
signal out0, out1, out2, out3                       : std_logic_vector(7 downto 0);
signal we                                           : std_logic;
signal mem_adr                                      : std_logic_vector(7 downto 0);
signal data_w                                       : std_logic_vector(7 downto 0);
signal statei2c                                     : std_logic_vector(2 downto 0);
signal clocki2c                                     : std_logic;
signal load, loadClk                                : std_logic_vector(3 downto 0);

signal clk120ToPLL, clk120ToPLL_buff                : std_logic;
signal codeOK_1, codeOK_2, serialOut_1, serialOut_2 : std_logic;
signal clk40_phased1, psdone1, phase_shift_PLL_locked1 : std_logic;
signal clk40_phased2, psdone2, phase_shift_PLL_locked2 : std_logic;
signal psen1, psen2                                 : std_logic;
signal phase_shift_count1, phase_shift_count2       : std_logic_vector(10 downto 0);
signal phase_shift1, phase_shift2                   : std_logic_vector(10 downto 0);
signal resetPLL1, resetPLL2                         : std_logic;  
signal counter                                      : std_logic_vector(5 downto 0);
signal laser_pulse1, laser_pulse2                   : std_logic;
signal codeOKneg_1, codeOKneg_2                     : std_logic;
signal codeOKpos_1, codeOKpos_2                     : std_logic;
signal codeOKresampled_1, codeOKresampled_2         : std_logic;
signal enable_pulse1, enable_pulse2                 : std_logic;
signal select_neg1, select_neg2                     : std_logic;
signal tx_addr, tx_trid, tx_ch, tx_cmd              : std_logic_vector(7 downto 0);
signal tx_data                                      : std_logic_vector(31 downto 0);
signal enable1, enable2  , disable1, disable2       : std_logic;
 
--LCD signals
signal DB_debug : std_logic_vector(7 downto 0);
signal RS_debug : std_logic;
signal RW_debug : std_logic;
signal E_debug : std_logic;
signal activeLSB_s : std_logic;

signal LCDPosition_s    : std_logic_vector(6 downto 0);
signal LCDCharacter_s   : std_logic_vector(7 downto 0);
signal LCDWrite_s       : std_logic;
signal LCDWrRdy_s       : std_logic;
signal LCDReady_s       : std_logic;

signal LCD_DB4_LS_s          : std_logic;
signal LCD_DB5_LS_s          : std_logic;
signal LCD_DB6_LS_s          : std_logic;
signal LCD_DB7_LS_s          : std_logic;
signal LCD_RW_LS_s           : std_logic;
signal LCD_RS_LS_s           : std_logic;
signal LCD_E_LS_s            : std_logic;

signal LCDTopWr_s : std_logic;
signal LCDTopRdy_s : std_logic;

signal LCDLine1 : LCDLine_arr (0 to 15);
signal LCDLine2 : LCDLine_arr (0 to 15);
signal State1_s : std_logic;
signal State2_s : std_logic;
signal State3_s : std_logic;
              
       -- ILA component  --
       --================--
       -- Vivado synthesis tool does not support mixed-language
       -- Solution: http://www.xilinx.com/support/answers/47454.html
       COMPONENT xlx_k7v7_vivado_debug PORT(
          CLK: in std_logic;
          PROBE0: in std_logic_vector(7 downto 0);
          PROBE1: in std_logic_vector(7 downto 0);
          PROBE2: in std_logic_vector(7 downto 0);
          PROBE3: in std_logic_vector(7 downto 0)
       );
       END COMPONENT;
              
      COMPONENT elink_ila PORT(
        CLK: in std_logic;
        PROBE0: in std_logic;
        PROBE1: in std_logic_vector(7 downto 0);
        PROBE2: in std_logic;
        PROBE3: in std_logic;
        PROBE4: in std_logic_vector(1 downto 0)
     );
     END COMPONENT;
       
       COMPONENT lcd_ila PORT(
         CLK: in std_logic;
         PROBE0: in std_logic_vector(7 downto 0);
         PROBE1: in std_logic_vector(6 downto 0);
         PROBE2: in std_logic;
         PROBE3: in std_logic;
         PROBE4: in std_logic;
         PROBE5: in std_logic;
         PROBE6: in std_logic
      );
      END COMPONENT;
              
       COMPONENT vio_lcd
        PORT (
          clk : IN STD_LOGIC;
          probe_out0 : OUT STD_LOGIC;
          probe_out1 : OUT STD_LOGIC;
          probe_out2
           : OUT STD_LOGIC;
          probe_in0 : IN STD_LOGIC
        );
      END COMPONENT;
              
       COMPONENT xlx_k7v7_vio
         PORT (
           clk : IN STD_LOGIC;
           probe_in0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
           probe_in1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
           probe_in2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
           probe_in3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
           probe_in4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
           probe_in5 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
           probe_in6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
           probe_in7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
           probe_in8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
           probe_in9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
           probe_in10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
           probe_in11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
           probe_in12 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
           probe_out0 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
           probe_out1 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
           probe_out2 : OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
           probe_out3 : OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
           probe_out4 : OUT STD_LOGIC;
           probe_out5 : OUT STD_LOGIC
         );
       END COMPONENT;
   
--=====================================================================================--  
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

--==================================== User Logic =====================================--
--=============--
--LCD Driver--
--=============--

LCD_DB4_LS          <= LCD_DB4_LS_s;
LCD_DB5_LS          <= LCD_DB5_LS_s;
LCD_DB6_LS          <= LCD_DB6_LS_s;
LCD_DB7_LS          <= LCD_DB7_LS_s;
LCD_RW_LS           <= LCD_RW_LS_s;
LCD_RS_LS           <= LCD_RS_LS_s;
LCD_E_LS            <= LCD_E_LS_s;

StateDebug_inst: entity work.State_Display 
    port map(
        clk_i               => clk40,
        reset_i             => reset_from_genRst,
            
        line1               => LCDLine1,
        line2               => LCDLine2,
        Write_o             => LCDTopWr_s,
        
        LCDRdy_i            => LCDTopRdy_s,
       -- State1              => State1_s,
        State1              => '1',
        State2              => '0',
        State3              => '0',
        out0                => out0,
        out1                => out1,
        out2                => out2,
        out3                => out3
    );   
        
lcdDebug_inst: lcd_ila
    PORT map(
         CLK        => clk40,
         PROBE0     => LCDCharacter_s,
         PROBE1     => LCDPosition_s,
         PROBE2     => LCDWrite_s,
         PROBE3     => LCDWrRdy_s,
         PROBE4     => E_debug,
         PROBE5     => LCD_RS_LS_s,
         PROBE6     => LCD_E_LS_s
      );

--LCDLine1 <= "bravo ca marche!";
--LCDLine2 <= "amuse-toi bien  ";

LCDTop_inst: entity work.LCDTop
    port map(
        clk_i               => clk40,
        reset_i             => reset_from_genRst,
            
        line1               => LCDLine1,
        line2               => LCDLine2,
        Write_i             => LCDTopWr_s,
        
        Rdy_o               => LCDTopRdy_s,
        
        LCDPosition_o       => LCDPosition_s,
        LCDCharcater_o      => LCDCharacter_s,
        Write_o             => LCDWrite_s,
        WriteRdy_i          => LCDWrRdy_s
    );        
lcdVIO_inst: vio_lcd
    PORT map(
      clk  => clk40,
      probe_out0 => State1_s,
      probe_out1 => State2_s,
      probe_out2 => State3_s,
      probe_in0 => LCDTopRdy_s
      
    );
    
LCDCtrl_inst: entity work.LCDCtrller
    generic map (
        CLK_FREQ            => 40
    )
    port map (
        reset_i             => reset_from_genRst,
        clk_i               => clk40,
        
        DB_o                => DB_debug,
        RS_o                => RS_debug,
        RW_o                => RW_debug,
        E_o                 => E_debug,
        activeLSB_o         => activeLSB_s,
        
        ready_o             => LCDReady_s,
        
        LCDPosition_i       => LCDPosition_s,
        LCDCharcater_i      => LCDCharacter_s,
        Write_i             => LCDWrite_s,
        WriteRdy_o          => LCDWrRdy_s
    );
        
LCDDriver_inst: entity  work.LCD_Driver 
    generic map (
        CLK_FREQ            => 40
    )
    port map(
        reset_i             => reset_from_genRst,
        clk_i               => clk40,
        
        DB_i                => DB_debug,
        RS_i                => RS_debug,
        RW_i                => RW_debug,
        E_i                 => E_debug,
        activeLSB_i         => activeLSB_s,
        
        LCD_DB4_LS          => LCD_DB4_LS_s,
        LCD_DB5_LS          => LCD_DB5_LS_s,
        LCD_DB6_LS          => LCD_DB6_LS_s,
        LCD_DB7_LS          => LCD_DB7_LS_s,
        LCD_RW_LS           => LCD_RW_LS_s,
        LCD_RS_LS           => LCD_RS_LS_s,
        LCD_E_LS            => LCD_E_LS_s
    );   
--=============--
-- SFP control -- 
--=============-- 

SFP_TX_DISABLE                                    <= '0';   

--===============--
-- General reset -- 
--===============--

genRst: entity work.xlx_k7v7_reset
   generic map (
      CLK_FREQ                                    => 156e6)
   port map (     
      CLK_I                                       => fabricClk_from_userClockIbufgds,
      RESET1_B_I                                  => not CPU_RESET, 
      RESET2_B_I                                  => not generalReset_from_user,
      RESET_O                                     => reset_from_genRst 
   ); 
reset_from_genRst_n <= not(reset_from_genRst);
--===============--
-- Clocks scheme -- 
--===============--   

-- Fabric clock:
----------------

-- Comment: USER_CLOCK frequency: 156MHz 

userClockIbufgds: ibufgds
   generic map (
      IBUF_LOW_PWR                                => FALSE,      
      IOSTANDARD                                  => "LVDS_25")
   port map (     
      O                                           => fabricClk_from_userClockIbufgds,   
      I                                           => USER_CLOCK_P,  
      IB                                          => USER_CLOCK_N 
   );

-- MGT(GTX) reference clock:
----------------------------

-- Comment: * The MGT reference clock MUST be provided by an external clock generator.
--
--          * The MGT reference clock frequency must be 120MHz for the latency-optimized GBT Bank. 

smaMgtRefClkIbufdsGtxe2: ibufds_gte2
   port map (
      O                                           => mgtRefClk_from_smaMgtRefClkIbufdsGtxe2,
      ODIV2                                       => open,
      CEB                                         => '0',
      I                                           => SMA_MGT_REFCLK_P,
      IB                                          => SMA_MGT_REFCLK_N
   );

--clk120ToPLL_bufh : BUFH
--    port map(
--    O               => clk120ToPLL,
--    I               => mgtRefClk_from_smaMgtRefClkIbufdsGtxe2
--    );

--clk120ToPLL_bufr : BUFR
--    port map(
--    O               => clk120ToPLL_buff,
--    CE              => '1',
--    CLR             => '0',
--    I               => clk120ToPLL
--    );

     

--==============--   
-- Test control --   
--==============--
  sysclk_inst: ibufds
      port map (
         I                                           => SYSCLK_P,
         IB                                          => SYSCLK_N,
         O                                           => sysclk
      ); 
 
txILa: xlx_k7v7_vivado_debug
               port map (
                  CLK => sysclk,
                  PROBE0 => out0,
                  PROBE1 => out1,
                  PROBE2 => out2,
                  PROBE3 => out3
                  );  
          
 
--=====================================================================================--   
-- ELINKS

	--=====================--     
   --elink monitoring from VLDB --     
   --=====================--
	--elink_din <= elink_dout;
	
	--=====================================--
		 -- HDMI 1: FMC_HPC_LA(20 to 24);
		 --=====================================--
		 
		 ibufgds_elink_clk1: IBUFGDS port map(
							  i               => FMC_HPC_LA20_p, 
							  ib 					=> FMC_HPC_LA20_n,
							  o             	=> elink_clk(5)
		 );
		 
		 ibufgds_elink_dout1: IBUFGDS port map(
							  i               => FMC_HPC_LA21_p,
							  ib 					=> FMC_HPC_LA21_n,
							  o             	=> elink_dout(5)
		 );
		 
		 ibufgds_elink_dio1: IBUFGDS port map(
							  i               => FMC_HPC_LA22_p, 
							  ib 					=> FMC_HPC_LA22_n, 
							  o             	=> elink_dio(5)
		 );
						 
		 obufds_elink1 : OBUFDS port map (
								O       			=> FMC_HPC_LA23_p,    
								OB 				=> FMC_HPC_LA23_n,  
								I         		=> elink_din(5)      
		);
						 
--=====================================--
-- HDMI 2: FMC_HPC_LA(15 to 19);
--=====================================--

ibufgds_elink_clk2: IBUFGDS port map(
i               => FMC_HPC_LA17_CC_P,
ib 			    => FMC_HPC_LA17_CC_N, 
o             	=> elink_clk(4)      -- 320MHz
);
		 
ibufgds_elink_dout2: IBUFGDS port map(
i               => FMC_HPC_LA16_P, 
ib 				=> FMC_HPC_LA16_N, 
o             	=> elink_dout(4)
);
		 
		 ibufgds_elink_dio2: IBUFGDS port map(
							  i               => FMC_HPC_LA18_CC_P, 
							  ib 					=> FMC_HPC_LA18_CC_N, 
							  o             	=> elink_dio(4)
		 );
						 
						 obufds_elink2 : OBUFDS port map (
			O       								=> FMC_HPC_LA15_p,   
			OB 									=> FMC_HPC_LA15_n,   
			I         							=> elink_din(4)      
		);
						 
--=====================================--
-- HDMI 3: FMC_HPC_LA(10 to 14);
--=====================================--

    ibufgds_elink_clk3: IBUFGDS port map(
				  i               => FMC_HPC_LA10_P,
				  ib              => FMC_HPC_LA10_N,
				  o               => elink_clk(3)
);
		 
    ibufgds_elink_dout3: IBUFGDS port map(
				  i               => FMC_HPC_LA11_P, 
				  ib              => FMC_HPC_LA11_N, 
				  o               => elink_dout(3)
);
		 
		 ibufgds_elink_dio3: IBUFGDS port map(
							  i               => FMC_HPC_LA13_p,
							  ib => FMC_HPC_LA13_n, 
							  o             => elink_dio(3)
		 );
						 
			obufds_elink3 : OBUFDS port map (
			O       					=> FMC_HPC_LA12_p,   
			OB 						=> FMC_HPC_LA12_n,  
			I         				=> elink_din(3)      
		);
						 
											  
--=====================================--
-- HDMI 4: FMC_HPC_LA(5 to 9);
--=====================================--

    ibufgds_elink_clk4: IBUFGDS port map(
i               => FMC_HPC_LA05_P, 
ib 				=> FMC_HPC_LA05_N, 
o             	=> elink_clk(2)
);

    ibufgds_elink_dout4: IBUFGDS port map(
i               => FMC_HPC_LA06_P,
ib 				=> FMC_HPC_LA06_N,
o             	=> elink_dout(2)
);
		 
		 ibufgds_elink_dio4: IBUFGDS port map(
							  i               => FMC_HPC_LA07_p,
							  ib 					=> FMC_HPC_LA07_n,
							  o             	=> elink_dio(2)
		 );
						 
			obufds_elink4 : OBUFDS port map (
							O       				=> FMC_HPC_LA08_p,    
							OB 					=> FMC_HPC_LA08_n,   
							I    			     	=> elink_din(2)      
		);          

--=====================================--
-- HDMI 5: FMC_HPC_LA(0 to 4);
--=====================================--
		 
ibufgds_elink_clk5: IBUFGDS port map(
i               => FMC_HPC_LA00_CC_P,
ib 				=> FMC_HPC_LA00_CC_N,
o             	=> elink_clk(1)  -- 320MHz
);
		 
ibufgds_elink_dout5: IBUFGDS port map(
i               => FMC_HPC_LA01_CC_P,
ib 				=> FMC_HPC_LA01_CC_N,
o             	=> elink_dout(1)
);
		 
		 ibufgds_elink_dio5: IBUFGDS port map(
							  i                 => FMC_HPC_LA03_p,
							  ib 				=> FMC_HPC_LA03_n,
							  o             	=> elink_dio(1)
		 );
		 
			obufds_elink5: OBUFDS port map (
								O       		=> FMC_HPC_LA02_p,    
								OB 				=> FMC_HPC_LA02_n,   
								I         		=> elink_din(1)      
		);         

selectBits_1 <= GPIO_DIP_SW1 & GPIO_DIP_SW0;
elink_des1: elink_des port map(
    dataIn320 => elink_dout(1),
    clk320 => elink_clk(1),
    dataOut40 => dataOut40_1,
    clk40 => clk40,
    selectBits => selectBits_1,
    codeOK => codeOK_1,
    serialOut => serialOut_1
    );

FMC_LPC_LA00_CC_P <= serialOut_1;

selectBits_2 <= GPIO_DIP_SW3 & GPIO_DIP_SW2;
elink_des2: elink_des port map(
    dataIn320 => elink_dout(4),
    clk320 => elink_clk(4),
    dataOut40 => dataOut40_2,
    clk40 => clk40,
    selectBits => selectBits_2,
    codeOK => codeOK_2,
    serialOut => serialOut_2
    ); 
    
FMC_LPC_LA08_P <= serialOut_2;

ibufgds_clk40: IBUFGDS port map(
i               => FMC_HPC_CLK0_M2C_P,
ib 				=> FMC_HPC_CLK0_M2C_N,
o             	=> clk40
);

--resetPLL <= reset_from_genRst or GPIO_SW_E;
phase_shift_PLL1: phase_shift_PLL port map(
   --Clock in ports
    clk_in1 => clk40,      
    -- Clock out ports
    clk_out1 => clk40_phased1,    
    -- Dynamic phase shift ports
    psclk => sysclk, 
    psen => psen1,
    psincdec => '1',  
    psdone => psdone1,     
    -- Status and control signals
    reset => resetPLL1,
    locked => phase_shift_PLL_locked1
    ); 

phase_shift_PLL2: phase_shift_PLL port map(
   --Clock in ports
    clk_in1 => clk40,      
    -- Clock out ports
    clk_out1 => clk40_phased2,    
    -- Dynamic phase shift ports
    psclk => sysclk, 
    psen => psen2,
    psincdec => '1',  
    psdone => psdone2,     
    -- Status and control signals
    reset => resetPLL2,
    locked => phase_shift_PLL_locked2
    ); 
--disable1 <= not(enable1);
disable1 <= not(out3(0));
phase_shift1 <= out1(2 downto 0) & out0(7 downto 0);
phaseShiftControl1 : phase_shift_control port map(
    --reset_in => GPIO_SW_E,
    reset_in => disable1,
    sysclk => sysclk, 
    psen => psen1,
    psdone => psdone1,
    max_count => phase_shift1,
    enable_pulse => enable_pulse1,
    select_neg => select_neg1,
    resetPLL => resetPLL1,
    PLLLocked => phase_shift_PLL_locked1
    );
    
--disable2 <= not(enable2);
disable2 <= not(out3(1));
phase_shift2 <= out2(5 downto 0) & out1(7 downto 3);
phaseShiftControl2 : phase_shift_control port map(
    --reset_in => GPIO_SW_E,
    reset_in => disable2,
    sysclk => sysclk, 
    psen => psen2,
    psdone => psdone2,
    max_count => phase_shift2,
    enable_pulse => enable_pulse2,
    select_neg => select_neg2,
    resetPLL => resetPLL2,
    PLLLocked => phase_shift_PLL_locked2
    );

process (clk40)
begin
    if(falling_edge(clk40)) then
        codeOKneg_1 <= codeOK_1;
        codeOKneg_2 <= codeOK_2;
    end if;
    if(rising_edge(clk40)) then
        codeOKpos_1 <= codeOK_1;
        codeOKpos_2 <= codeOK_2;
    end if;
end process;

codeOKresampled_1 <= codeOKneg_1 when (select_neg1 = '1') else codeOKpos_1;
codeOKresampled_2 <= codeOKneg_2 when (select_neg2 = '1') else codeOKpos_2;

process (clk40_phased1)
begin
    if(rising_edge(clk40_phased1)) then
        if (enable_pulse1 = '1') then
            laser_pulse1 <= codeOKresampled_1;
        else 
            laser_pulse1 <= '0';
        end if;
    end if;
end process;
FMC_LPC_LA02_P <= laser_pulse1;

process (clk40_phased2)
begin
    if(rising_edge(clk40_phased2)) then 
        if (enable_pulse2 = '1') then
            laser_pulse2 <= codeOKresampled_2;
        else
            laser_pulse2 <= '0';
        end if;
    end if;
end process;
FMC_LPC_LA01_CC_P <= laser_pulse2;

IOBUF_inst : IOBUF
generic map (
DRIVE => 12,
IOSTANDARD => "DEFAULT",
SLEW => "SLOW")
port map (
O => SDA_in, -- Buffer output
IO => USER_SMA_CLOCK_P, -- Buffer inout port (connect directly to top-level port)
I => SDA_out, --'0', -- Buffer input
T => SDA_en_b -- 3-state enable input, high=input, low=output
);

SDA_en_b <= not(SDA_en);

SCL_in <= USER_SMA_CLOCK_N;

i2c_config_inst : i2c_config port map ( 
    SCL => SCL_in,
    reset => reset_from_genRst,
    refClk => sysclk,
    SDAen => SDA_en,
    SDAo  => SDA_out,
    SDAi => SDA_in,
    chip_id => "0110011",
    out0 => out0,
    out1 => out1,
    out2 => out2,
    out3 => out3
    );
    
elink_debug_ila_inst: elink_ila 
    PORT MAP(
        CLK     => elink_clk(1),
        PROBE0  => elink_dout(1),
        PROBE1  => dataOut40_1,
        PROBE2  => serialOut_1,
        PROBE3  => codeOK_1,
        PROBE4  => selectBits_1
     );
     
--USER_SMA_GPIO_P <= codeOK_2;
--USER_SMA_GPIO_N <= laser_pulse2;
USER_SMA_GPIO_P <= laser_pulse1;
USER_SMA_GPIO_N <= laser_pulse2;

end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--