--============================================================================--
--#######################   Module Information   #############################--
--============================================================================--
--
-- Company          : CERN (EP-ESE-BE)
-- Engineer         : Julian Mendez <julian.mendez@cern.ch>
--
-- Project Name     : GBT-SC module (IC and EC fields)
-- Module Name      : IC_top
--
-- Language         : VHDL
--
-- Target Device    : Device agnostic
-- Tool version     : -
--
-- Version          : 1.0
--
-- Description      : GBTx internal control - TOP level
--
-- Versions history : DATE      VERS.   AUTHOR      DESCRIPTION
--
--                    05/04/17  1.0     J. Mendez   First .vhd module definition
--
-- Add. Comments    : -
--                                                                              
--============================================================================--
--############################################################################--
--============================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--============================================================================--
--#############################   Entity   ###################################--
--============================================================================--
entity ic_top is
    generic (
        g_FIFO_DEPTH        : integer := 10
    );
    port (
        -- Clock and reset
        tx_clk_i            : in  std_logic;
        rx_clk_i            : in  std_logic;
        reset_i             : in  std_logic;
        
        -- Status
        tx_ready_o          : out std_logic;
        rx_empty_o          : out std_logic;
        
        rx_gbtx_addr_o      : out std_logic_vector(7 downto 0);
        rx_mem_ptr_o        : out std_logic_vector(15 downto 0);
        rx_nb_of_words_o    : out std_logic_vector(15 downto 0);
        
        -- Configuration
        tx_GBTx_address_i   : in  std_logic_vector(7 downto 0);
        tx_register_addr_i  : in  std_logic_vector(15 downto 0);
        tx_nb_to_be_read_i  : in  std_logic_vector(15 downto 0);
        
        -- Internal FIFO
        tx_wr_i             : in  std_logic;
        tx_data_to_gbtx_i   : in  std_logic_vector(7 downto 0); 
        
        rx_rd_i             : in  std_logic;
        rx_data_from_gbtx_o : out std_logic_vector(7 downto 0);
        
        -- FSM Control
        tx_start_write_i    : in  std_logic;
        tx_start_read_i     : in  std_logic;
        
        -- IC lines
        tx_data_o           : out std_logic_vector(1 downto 0);
        rx_data_i           : in  std_logic_vector(1 downto 0)
    );   
end ic_top;

--============================================================================--
--##########################   Architecture   ################################--
--============================================================================--
architecture behaviour of ic_top is

    -- Signals
    signal data_ic_tx_inv       : std_logic_vector(1 downto 0);
    signal data_ic_rx_inv       : std_logic_vector(1 downto 0);
    
    -- Components
    component ic_tx
        generic (
            g_FIFO_DEPTH        : integer := 10
        );
        port (
            -- Clock and reset
            tx_clk_i            : in std_logic;
            reset_i             : in std_logic;
            
            -- Status
            tx_ready_o          : out std_logic;
            
            -- Configuration
            GBTx_address_i      : in std_logic_vector(7 downto 0);
            Register_addr_i     : in std_logic_vector(15 downto 0);
            nb_to_be_read_i     : in std_logic_vector(15 downto 0);
            
            -- Internal FIFO
            wr_i                : in std_logic;
            data_i              : in std_logic_vector(7 downto 0);
            
            -- FSM Control
            start_write         : in std_logic;
            start_read          : in std_logic;
            
            -- IC line
            tx_data_o           : out std_logic_vector(1 downto 0)
        );   
    end component ic_tx;
    
    component ic_rx is
        generic (
            g_FIFO_DEPTH:  integer := 10
        );
        port (
            -- Clock and reset
            rx_clk_i        : in  std_logic;
            reset_i         : in  std_logic;

            -- Status
            empty_o         : out std_logic;

            gbtx_addr_o     : out std_logic_vector(7 downto 0);
            mem_ptr_o       : out std_logic_vector(15 downto 0);
            nb_of_words_o   : out std_logic_vector(15 downto 0);

            -- Internal FIFO
            rd_i            : in  std_logic;
            data_o          : out std_logic_vector(7 downto 0);

            -- IC lines
            rx_data_i       : in std_logic_vector(1 downto 0)    
       );   
    end component ic_rx;

begin       --========####   Architecture Body   ####========-- 

    tx_data_o(0)            <= data_ic_tx_inv(1);
    tx_data_o(1)            <= data_ic_tx_inv(0);
    
    data_ic_rx_inv(0)       <= rx_data_i(1);
    data_ic_rx_inv(1)       <= rx_data_i(0);
    
    -- TX
    tx_inst: ic_tx
        generic map (
            g_FIFO_DEPTH    => g_FIFO_DEPTH
        )
        port map (
            -- Clock and reset
            tx_clk_i        => tx_clk_i,
            reset_i         => reset_i,

            -- Status
            tx_ready_o      => tx_ready_o,

            -- Configuration
            GBTx_address_i  => tx_GBTx_address_i,
            Register_addr_i => tx_register_addr_i,
            nb_to_be_read_i => tx_nb_to_be_read_i,

            -- FSM Control
            start_write     => tx_start_write_i,
            start_read      => tx_start_read_i,
            
            -- Internal FIFO
            wr_i            => tx_wr_i,
            data_i          => tx_data_to_gbtx_i,

            -- IC line
            tx_data_o       => data_ic_tx_inv
        );

    -- RX
    rx_inst: entity work.ic_rx
        generic map (
            g_FIFO_DEPTH    => g_FIFO_DEPTH
        )
        port map (
            -- Clock and reset
            rx_clk_i        => rx_clk_i,
            reset_i         => reset_i,

            -- Status
            empty_o         => rx_empty_o,
            
            gbtx_addr_o     => rx_gbtx_addr_o,
            mem_ptr_o       => rx_mem_ptr_o,
            nb_of_words_o   => rx_nb_of_words_o,
            
            -- Internal FIFO
            rd_i            => rx_rd_i,
            data_o          => rx_data_from_gbtx_o,
        
            -- IC line
            rx_data_i       => data_ic_rx_inv
        );    

end behaviour;
--============================================================================--
--############################################################################--
--============================================================================--