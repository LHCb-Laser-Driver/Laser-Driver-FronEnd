`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.07.2017 14:59:13
// Design Name: 
// Module Name: test_not
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_not(
    input dataIn320,
    output Y,
    input clk
    );
    reg[9:0] shiftReg;
    //assign Y = ~A;
    //always @(posedge clk) Y <= A;
    always @(posedge clk) shiftReg[9:0] <= {shiftReg[8:0],dataIn320};
    assign Y = shiftReg[9];
endmodule
