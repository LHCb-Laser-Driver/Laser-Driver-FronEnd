`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Ken
// 
// Create Date: 25.08.2017 10:55:51
// Design Name: 
// Module Name: sca_driver
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sca_driver(
    input reset,
    input clock,
    input start_in,
    output reg startConnect,
    output reg startTransaction,
    output reg[7:0] tx_addr, 
    output reg[7:0] tx_trid, 
    output reg[7:0] tx_ch,   
    output reg[7:0] tx_cmd,  
    output reg[31:0] tx_data,
    input[7:0] rx_trid,
    output reg[7:0] state,
    input[7:0] byte0,
    input[7:0] byte1,
    input[7:0] byte2,
    input[7:0] byte3
    );
    
reg start_reg1, start_reg2;
wire start;   
reg[7:0] next_state;
reg[7:0] connectCount, waitConnectCount;

`define idleState			    8'h00
`define sendResetState			8'h01
`define waitResetState			8'h02
`define sendConnectState		8'h03 
`define waitConnectState		8'h04
`define writeControllerState		8'h05
`define waitControllerState		8'h06
`define writeCRAState			8'h07
`define waitCRAState			8'h08
`define write_multi_4byte0State		8'h09
`define wait_multi_4byte0State		8'h0a		
`define write_multi_4byte1State		8'h0b
`define wait_multi_4byte1State		8'h0c
`define writeMultiState			8'h0d
`define waitMultiState			8'h0e
`define write1Byte			8'h0f
`define waitWrite1Byte			8'h10
`define read1Byte			8'h11
`define waitRead1Byte			8'h12
`define readMulti			8'h13
`define waitReadMulti			8'h14
`define read_multi_4byte3State		8'h15
`define waitread_multi_4byte3State	8'h16
`define read_multi_4byte2State		8'h17
`define waitread_multi_4byte2State	8'h18  

always @(posedge clock)
begin
    start_reg1 <= start_in;
    start_reg2 <= start_reg1;
end

assign start = start_reg1 & ~start_reg2; 
    
// FSM for controlling first PLL
always @(posedge clock or posedge reset)
begin
    if (reset) 
    begin
        state <= `idleState;
    end
    else
    begin
        state <= next_state;
    end
    case(state)
    `idleState:
    begin
        connectCount <= 8'h00;
        waitConnectCount <= 8'h00;
        startConnect <= 1'b0;
        startTransaction <= 1'b0;
        tx_addr <= 8'h00; 
        tx_trid <= 8'h00; 
        tx_ch <= 8'h00; 
        tx_cmd <= 8'h00; 
        tx_data <= 32'h00000000;      
    end
    `sendConnectState:
    begin
        connectCount <= connectCount + 1'b1;
        waitConnectCount <= 8'h00;
        startConnect <= 1'b1;
        startTransaction <= 1'b0;
        tx_addr <= 8'h00; 
        tx_trid <= 8'h00; 
        tx_ch <= 8'h00; 
        tx_cmd <= 8'h00; 
        tx_data <= 32'h00000000; 
    end
    `waitConnectState:
    begin
        connectCount <= 8'h00;
        waitConnectCount <= waitConnectCount + 1'b1;
        startConnect <= 1'b0; 
        startTransaction <= 1'b0;
        tx_addr <= 8'h00; 
        tx_trid <= 8'h00; 
        tx_ch <= 8'h00; 
        tx_cmd <= 8'h00; 
        tx_data <= 32'h00000000; 
    end
    `writeControllerState:
    begin
         connectCount <= 8'h00;
         waitConnectCount <= 8'h00;
         startConnect <= 1'b0; 
         startTransaction <= 1'b1;
         tx_addr <= 8'h00; 
         tx_trid <= 8'h01; 
         tx_ch <= 8'h00; 
         tx_cmd <= 8'h02; 
         tx_data <= 32'hFF000000;            
    end
    `waitControllerState:    // 6
    begin
        connectCount <= 8'h00;
        waitConnectCount <= 8'h00;
        startConnect <= 1'b0; 
        startTransaction <= 1'b0;
        tx_addr <= 8'h00; 
        tx_trid <= 8'h01; 
        tx_ch <= 8'h00; 
        tx_cmd <= 8'h02; 
        tx_data <= 32'hFF000000;
    end
    `writeCRAState:    // 7
    begin
        connectCount <= 8'h00;
        waitConnectCount <= 8'h00;
        startConnect <= 1'b0; 
        startTransaction <= 1'b1;
        tx_addr <= 8'h00; 
        tx_trid <= 8'h02; 
        tx_ch <= 8'h03; 
        tx_cmd <= 8'h30; 
        tx_data <= {1'b0,5'b00101,2'b00, 24'h000000}; //SCLMODE[7] NBYTE[6:2]  FREQ[1:0]
    end
    `waitCRAState:    //8
    begin
        connectCount <= 8'h00;
        waitConnectCount <= 8'h00;
        startConnect <= 1'b0; 
        startTransaction <= 1'b0;
        tx_addr <= 8'h00; 
        tx_trid <= 8'h02; 
        tx_ch <= 8'h03; 
        tx_cmd <= 8'h30; 
        tx_data <= {1'b0,5'b00101,2'b00, 24'h000000}; //SCLMODE[7] NBYTE[6:2]  FREQ[1:0]        
    end
    `write_multi_4byte0State:    //9
    begin
        connectCount <= 8'h00;
        waitConnectCount <= 8'h00;
        startConnect <= 1'b0; 
        startTransaction <= 1'b1;
        tx_addr <= 8'h00; 
        tx_trid <= 8'h03; 
        tx_ch <= 8'h03; 
        tx_cmd <= 8'h40; 
        tx_data <= {8'h00,byte0,byte1,byte2};
    end
    `wait_multi_4byte0State:	//a
    begin
        connectCount <= 8'h00;
        waitConnectCount <= 8'h00;
        startConnect <= 1'b0; 
        startTransaction <= 1'b0;
        tx_addr <= 8'h00; 
        tx_trid <= 8'h03; 
        tx_ch <= 8'h03; 
        tx_cmd <= 8'h40; 
        tx_data <= {8'h00,byte0,byte1,byte2};    
    end
    `write_multi_4byte1State:    //b
    begin
        connectCount <= 8'h00;
        waitConnectCount <= 8'h00;
        startConnect <= 1'b0; 
        startTransaction <= 1'b1;
        tx_addr <= 8'h00; 
        tx_trid <= 8'h04; 
        tx_ch <= 8'h03; 
        tx_cmd <= 8'h50; 
        tx_data <= {byte3,8'h00,8'h00,8'h00};
    end
    `wait_multi_4byte1State:	//c
    begin
        connectCount <= 8'h00;
        waitConnectCount <= 8'h00;
        startConnect <= 1'b0; 
        startTransaction <= 1'b0;
        tx_addr <= 8'h00; 
        tx_trid <= 8'h04; 
        tx_ch <= 8'h03; 
        tx_cmd <= 8'h50; 
        tx_data <= {byte3,8'h00,8'h00,8'h00};    
    end
    `writeMultiState:			//d
    begin
        connectCount <= 8'h00;
        waitConnectCount <= 8'h00;
        startConnect <= 1'b0; 
        startTransaction <= 1'b1;
        tx_addr <= 8'h00; 
        tx_trid <= 8'h05; 
        tx_ch <= 8'h03; 
        tx_cmd <= 8'hDA; 
        tx_data <= {8'h33,8'h00,8'h00,8'h00};   // i2c slave address[7:0]    
    end
        `waitMultiState:            //e
    begin
        connectCount <= 8'h00;
        waitConnectCount <= 8'h00;
        startConnect <= 1'b0; 
        startTransaction <= 1'b0;
        tx_addr <= 8'h00; 
        tx_trid <= 8'h05; 
        tx_ch <= 8'h03; 
        tx_cmd <= 8'hDA; 
        tx_data <= {8'h33,8'h00,8'h00,8'h00};          
    end
    default:
    begin
        connectCount <= 8'h00;
        waitConnectCount <= 8'h00;
        startConnect <= 1'b0;
    end
    endcase
end

always @*
begin
    case(state)
    `idleState:
    begin
        if (reset) next_state       <= `idleState;
        else if (start) next_state  <= `sendConnectState;
        else next_state             <= `idleState;
    end
    `sendConnectState:
    begin
        if (reset) next_state       <= `idleState;
        else if (connectCount >= 4) next_state <= `waitConnectState;
        else next_state <= `sendConnectState;
    end
    `waitConnectState:
    begin
        if (reset) next_state       <= `idleState;
        else if (waitConnectCount >= 200) next_state         <= `writeControllerState;
        else next_state             <= `waitConnectState;
    end
    `writeControllerState:
    begin
        if (reset) next_state       <= `idleState;
        else next_state <= `waitControllerState; 
    end
    `waitControllerState:    // 6
    begin
        if (reset) next_state       <= `idleState;
        else if (rx_trid ==  tx_trid) next_state <= `writeCRAState;
        else  next_state <= `waitControllerState;
    end
    `writeCRAState:    // 7
    begin
        if (reset) next_state       <= `idleState;
        else next_state <= #1 `waitCRAState;
    end
    `waitCRAState:    //8
    begin
        if (reset) next_state       <= `idleState; 
        else if (rx_trid ==  tx_trid) next_state <= `write_multi_4byte0State;
        else  next_state <= `waitCRAState;   
    end
    `write_multi_4byte0State:    //9
    begin
        if (reset) next_state       <= `idleState;  
        else next_state <= `wait_multi_4byte0State; 
    end
    `wait_multi_4byte0State:
    begin
        if (reset) next_state       <= `idleState; 
        else if (rx_trid ==  tx_trid) next_state <= `write_multi_4byte1State;
        else  next_state <= `wait_multi_4byte0State;    
    end
    ///////////////////////////////////////////////
    `write_multi_4byte1State:    //
    begin
        if (reset) next_state       <= `idleState;  
        else next_state <= `wait_multi_4byte1State; 
    end
    `wait_multi_4byte1State:
    begin
        if (reset) next_state       <= `idleState; 
        else if (rx_trid ==  tx_trid) next_state <= `writeMultiState;
        else  next_state <= `wait_multi_4byte1State;    
    end
    /////////////////////////////////////////////////
    `writeMultiState:			//d
    begin
        if (reset) next_state       <= `idleState;  
        else next_state <= `waitMultiState; 
    end
    `waitMultiState:            //e
    begin
        if (reset) next_state       <= `idleState; 
        else if (rx_trid ==  tx_trid) next_state <= `idleState;
        else  next_state <= `waitMultiState; 
    end
    default:
    begin
        next_state       <= `idleState;
    end
    endcase
end    

endmodule
