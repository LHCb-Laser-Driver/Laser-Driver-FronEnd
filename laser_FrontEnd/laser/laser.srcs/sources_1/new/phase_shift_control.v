`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Ken
// 
// Create Date: 18.08.2017 14:27:29
// Design Name: 
// Module Name: phase_shift_control
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module phase_shift_control(
    input reset_in,
    input sysclk,
    output reg psen,
    input psdone,
    input[10:0] max_count,   // PLL should allow 1400 phase shifts
    output reg enable_pulse,
    output reg select_neg,
    output reg resetPLL,
    input PLLLocked
    );
reg[2:0] state, next_state;
reg[11:0] counter;

`define resetState   3'h0
`define waitPLLLockState 3'h1
`define psenState  3'h2
`define waitState  3'h3
`define enableState  3'h4

// FSM for controlling first PLL
always @(posedge sysclk)
begin
    if (reset_in) 
    begin
        state <= `resetState;
    end
    else
    begin
        state <= next_state;
    end
    case(state)
    `resetState:
    begin
        psen <= 1'b0;
        counter <= 12'h000;
        enable_pulse <= 1'b0;
        select_neg <= select_neg;
        resetPLL <= 1'b1;
    end
    `waitPLLLockState:
    begin
        psen <= 1'b0;
        counter <= 12'h000;
        enable_pulse <= 1'b0;
        select_neg <= select_neg;
        resetPLL <= 1'b0;
    end
    `psenState:
    begin
        psen <= 1'b1;
        counter <= counter;
        enable_pulse <= 1'b0;
        select_neg <= (max_count <= 11'd350);
        resetPLL <= 1'b0;
    end
    `waitState:
    begin
        psen <= 1'b0;
        if (psdone) counter <= counter + 1'b1;
        else counter <= counter;
        enable_pulse <= 1'b0;
        select_neg <= select_neg;
        resetPLL <= 1'b0;
    end
    `enableState:
    begin
        psen <= 1'b0;
        counter <= counter;
        enable_pulse <= 1'b1;
        select_neg <= select_neg;
        resetPLL <= 1'b0;
    end
    default:
    begin
        psen <= 1'b0;
        counter <= 12'h000;
        enable_pulse <= 1'b0;
        select_neg <= select_neg;
        resetPLL <= 1'b1;
    end
    endcase
end

always @*
begin
    case(state)
    `resetState:
    begin
        if (reset_in) next_state       <= `resetState;
        else next_state             <= `waitPLLLockState;
    end
    `waitPLLLockState:
    begin
        if (reset_in) next_state       <= `resetState;
        else if (PLLLocked) next_state  <= `psenState;
        else next_state             <= `waitPLLLockState;
    end
    `psenState:
    begin
        if (reset_in) next_state       <= `resetState;
        else next_state             <= `waitState;
    end
    `waitState:
    begin
        if (reset_in) next_state       <= `resetState;
        else if (psdone)
        begin
            if (counter >= max_count) next_state <= `enableState;
            else next_state         <= `psenState;
        end
        else next_state             <= `waitState;
    end
    `enableState:
    begin
        if (reset_in) next_state       <= `resetState;
        else next_state             <= `enableState;
    end
    default:
    begin
        next_state       <= `resetState;
    end
    endcase
end

endmodule
