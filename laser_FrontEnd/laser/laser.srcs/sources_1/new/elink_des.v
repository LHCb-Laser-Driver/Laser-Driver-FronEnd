`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Ken
// 
// Create Date: 13.07.2017 16:55:37
// Design Name: 
// Module Name: elink_des
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module elink_des(
    dataIn320,
    clk320,
    dataOut40,
    clk40,
    selectBits,
    codeOK,
    serialOut
    );

input dataIn320;
input clk320;
output [7:0] dataOut40;
input clk40;
input [1:0] selectBits;
output codeOK;
output serialOut;
    
reg[9:0] shiftReg;
reg[7:0] dataOut40;
wire[7:0] dataInt;
reg codeOK;

always @(posedge clk320) shiftReg[9:0] <= {shiftReg[8:0],dataIn320};

assign dataInt =    (selectBits == 2'b00) ? shiftReg[7:0] :
                    (selectBits == 2'b01) ? shiftReg[8:1] :
                    (selectBits == 2'b10) ? shiftReg[9:2] :
                    8'h00;
                    
always @(posedge clk40)  dataOut40 <= dataInt;
//always @(posedge clk40) codeOK <= (dataOut40 == 8'hAA);
always @(posedge clk40) codeOK <= (dataOut40[1] == 1'b1);

assign serialOut = dataInt[7];
endmodule
