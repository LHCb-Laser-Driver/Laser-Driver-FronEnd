--============================================================================--
--#######################   Module Information   #############################--
--============================================================================--
--
-- Company          : CERN (EP-ESE-BE)
-- Engineer         : Julian Mendez <julian.mendez@cern.ch>
--
-- Project Name     : GBT-SC module (IC and EC fields)
-- Module Name      : SCA_PKG
--
-- Language         : VHDL
--
-- Target Device    : Device agnostic
-- Tool version     : -
--
-- Version          : 1.0
--
-- Description      : SCA control - SCA Package (types and CRC func.)
--
-- Versions history : DATE      VERS.   AUTHOR      DESCRIPTION
--
--                    06/04/17  1.0     J. Mendez   First .vhd module definition
--
-- Add. Comments    : -
--                                                                              
--============================================================================--
--############################################################################--
--============================================================================--

-- IEEE VHDL standard library:
library IEEE;
use IEEE.std_logic_1164.all;

--============================================================================--
--#############################   Package   ##################################--
--============================================================================--
package LCD_PKG is

    -- Types & Subtypes
    type characterList_t is (
        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        'g',
        'h',
        'i',
        'j',
        'k',
        'l',
        'm',
        'n',
        'o',
        'p',
        'q',
        'r',
        's',
        't',
        'u',
        'v',
        'w',
        'x',
        'y',
        'z',
        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        ' ',
        '-',
        '!');
    
    type LCDLine_arr  is array(integer range <>) of characterList_t;

    -- Functions
    function characterToHexa (
        char: in characterList_t)
    return std_logic_vector;
    
     function HexToASCII (
           reg: in std_logic_vector(7 downto 0))
       return LCDLine_arr;
    
end LCD_PKG;

--============================================================================--
--##############################   Body   ####################################--
--============================================================================--
package body LCD_PKG is

    -- Vector reverse function
    function characterToHexa (
        char: in characterList_t)
    return std_logic_vector is
      
    begin    
        
        case char is
            when 'a' =>	 return "01100001";
            when 'b' =>    return "01100010";
            when 'c' =>    return "01100011";
            when 'd' =>    return "01100100";
            when 'e' =>    return "01100101";
            when 'f' =>    return "01100110";
            when 'g' =>    return "01100111";
            when 'h' =>    return "01101000";
            when 'i' =>    return "01101001";
            when 'j' =>    return "01101010";
            when 'k' =>    return "01101011";
            when 'l' =>    return "01101100";
            when 'm' =>    return "01101101";
            when 'n' =>    return "01101110";
            when 'o' =>    return "01101111";
            when 'p' =>    return "01110000";
            when 'q' =>    return "01110001";
            when 'r' =>    return "01110010";
            when 's' =>    return "01110011";
            when 't' =>    return "01110100";
            when 'u' =>    return "01110101";
            when 'v' =>    return "01110110";
            when 'w' =>    return "01110111";
            when 'x' =>    return "01111000";
            when 'y' =>    return "01111001";
            when 'z' =>    return "01111010";
            
            when '0' =>    return "00110000";
            when '1' =>    return "00110001";
            when '2' =>    return "00110010";
            when '3' =>    return "00110011";
            when '4' =>    return "00110100";
            when '5' =>    return "00110101";
            when '6' =>    return "00110110";
            when '7' =>    return "00110111";
            when '8' =>    return "00111000";
            when '9' =>    return "00111001";
            
            when ' ' => return "00100000";
            when '-' => return "00101101";
            when '!' => return "00100001";
        end case;
         
        return "00000000";
    end;
    
    function HexToASCII (
           reg: in std_logic_vector(7 downto 0))
           return LCDLine_arr is
           variable character_s: LCDLine_arr(0 to 1);
   begin
                    
           case reg(7 downto 4) is
                           
                           when x"0"           =>           character_s(0) := '0';
                           when x"1"           =>           character_s(0) := '1';
                           when x"2"           =>           character_s(0) := '2';
                           when x"3"           =>           character_s(0) := '3';
                           when x"4"           =>           character_s(0) := '4';
                           when x"5"           =>           character_s(0) := '5';
                           when x"6"           =>           character_s(0) := '6';
                           when x"7"           =>           character_s(0) := '7';
                           when x"8"           =>           character_s(0) := '8';
                           when x"9"           =>           character_s(0) := '9';
                           when x"A"           =>           character_s(0) := 'a';
                           when x"B"           =>           character_s(0) := 'b';
                           when x"C"           =>           character_s(0) := 'c';
                           when x"D"           =>           character_s(0) := 'd';
                           when x"E"           =>           character_s(0) := 'e';
                           when x"F"           =>           character_s(0) := 'f';
           
           end case;
           
           case reg(3 downto 0) is
                           
                           when x"0"           =>           character_s(1) := '0';
                           when x"1"           =>           character_s(1) := '1';
                           when x"2"           =>           character_s(1) := '2';
                           when x"3"           =>           character_s(1) := '3';
                           when x"4"           =>           character_s(1) := '4';
                           when x"5"           =>           character_s(1) := '5';
                           when x"6"           =>           character_s(1) := '6';
                           when x"7"           =>           character_s(1) := '7';
                           when x"8"           =>           character_s(1) := '8';
                           when x"9"           =>           character_s(1) := '9';
                           when x"A"           =>           character_s(1) := 'a';
                           when x"B"           =>           character_s(1) := 'b';
                           when x"C"           =>           character_s(1) := 'c';
                           when x"D"           =>           character_s(1) := 'd';
                           when x"E"           =>           character_s(1) := 'e';
                           when x"F"           =>           character_s(1) := 'f';
           
           end case;
           
           return character_s;
                   
   end;        
    
end LCD_PKG;
