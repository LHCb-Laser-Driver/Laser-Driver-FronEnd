-------------------------------------------------------
--! @file
--! @author Bruno Allongue (CERN EP-ESE-FE)
--! @date November, 2017
--! @version 1.0
--! @brief LCD Controller - Init state machine and data management
-------------------------------------------------------

--! Include the IEEE VHDL standard library
library ieee;
--! Use STD_Logic to define vector types
use ieee.std_logic_1164.all;

--! @brief LCD_Driver Entity - Driver module
entity LCDCtrller is
    generic (
        CLK_FREQ            : integer
    );
    port (
        reset_i             : in  std_logic;
        clk_i               : in  std_logic;                        --! Clock used for the internal state machine
        
        DB_o                : out std_logic_vector(7 downto 0);     --! DB bus (LCD pins)
        RS_o                : out std_logic;                        --! Register select (0: Instruction reg / 1: Data register)
        RW_o                : out std_logic;                        --! Read/Write command (0: Write / 1: Read)
        E_o                 : out std_logic;                        --! Start command (rising edge detection)
        activeLSB_o         : out std_logic;
        
        ready_o             : out std_logic;
        
        LCDPosition_i       : in  std_logic_vector(6 downto 0);
        LCDCharcater_i      : in  std_logic_vector(7 downto 0);
        Write_i             : in  std_logic;
        WriteRdy_o          : out std_logic
    );   
end LCDCtrller;

--! @brief LCD_Driver Architecture - Driver module
architecture behaviour of LCDCtrller is

    -- Types
    type initFSMstate_t is ( s0_delay40ms,
                            s1_send4bFn,
                            s2_delay40ms,
                            s3_sendConfig,
                            s4_delay40ms,
                            s5_sendDisplayOn, 
                            s6_delay40ms, 
                            s7_sendClear, 
                            s8_wait40ms,
                            s9_ready);
                            
    type wrFSMstate_t is ( s0_waitForWriteAndSendAddr,
                            s1_delay40ms,
                            s2_sendData,
                            s3_delay40ms);
                            
    
    -- Signal
    signal initFSMstate_s       : initFSMstate_t;
    signal writeFSMstate_s      : wrFSMstate_t;
    
    signal ready_s              : std_logic;
    signal write_s0             : std_logic;
    
    signal E_sInit              : std_logic;
    signal DB_sInit             : std_logic_vector(7 downto 0);
    signal RS_sInit             : std_logic;
    signal RW_sInit             : std_logic;
    signal activeLSB_sInit      : std_logic;
        
    signal E_sWrite             : std_logic;
    signal DB_sWrite            : std_logic_vector(7 downto 0);
    signal RS_sWrite            : std_logic;
    signal RW_sWrite            : std_logic;
    signal activeLSB_sWrite     : std_logic;
    
begin       --========####   Architecture Body   ####========-- 

    ready_o <= ready_s;
    
    DB_o                <= DB_sInit when ready_s = '0' else
                           DB_sWrite;
                           
    RS_o                <= RS_sInit when ready_s = '0' else
                           RS_sWrite;
                           
    RW_o                <= RW_sInit when ready_s = '0' else
                           RW_sWrite;
    
    E_o                 <= E_sInit when ready_s = '0' else
                           E_sWrite;
    
    activeLSB_o         <= activeLSB_sInit when ready_s = '0' else
                           activeLSB_sWrite;
    
    write_proc: process(clk_i, reset_i)
        variable cnter:  integer range 0 to (1+((CLK_FREQ *  40000)));
    begin
    
        if reset_i = '1' then
        
            writeFSMstate_s <= s0_waitForWriteAndSendAddr;
            write_s0        <= Write_i;
            
            E_sWrite              <= '0';
            DB_sWrite             <= "00000000";
            RS_sWrite             <= '0';
            RW_sWrite             <= '0';
            activeLSB_sWrite      <= '0';
            
            WriteRdy_o            <= '0';
                       
        elsif rising_edge(clk_i) then
        
            Write_s0    <= Write_i;
            E_sWrite    <= '0';
            
            case writeFSMstate_s is 
            
                when s0_waitForWriteAndSendAddr =>  if ready_s = '1' then
                                                        WriteRdy_o <= '1';
                                                    end if;
                                                    
                                                    if ready_s = '1' and Write_i = '1' and Write_s0 = '0' then
                                                    
                                                        WriteRdy_o            <= '0';
                                                        
                                                        E_sWrite              <= '1';
                                                        DB_sWrite             <= '1' & LCDPosition_i;
                                                        RS_sWrite             <= '0';
                                                        RW_sWrite             <= '0';
                                                        activeLSB_sWrite      <= '1';
                            
                                                        cnter               := 0;
                                                        
                                                        writeFSMstate_s <= s1_delay40ms;
                                                        
                                                    end if;
                
                when s1_delay40ms =>                --! Count until counter >= (CLK_FREQ * 4ms)
                                                    cnter := cnter + 1;
                                                    
                                                    if cnter >= (1+((CLK_FREQ * 1000))) then
                                                        writeFSMstate_s <= s2_sendData;
                                                    end if;
                
                when s2_sendData =>                 E_sWrite              <= '1';
                                                    DB_sWrite             <= LCDCharcater_i;
                                                    RS_sWrite             <= '1';
                                                    RW_sWrite             <= '0';
                                                    activeLSB_sWrite      <= '1';
                                                    
                                                    cnter               := 0;
                                                    
                                                    writeFSMstate_s <= s3_delay40ms;
                
                when s3_delay40ms =>                --! Count until counter >= (CLK_FREQ * 4ms)
                                                    cnter := cnter + 1;
                                                    
                                                    if cnter >= (1+((CLK_FREQ * 1000))) then
                                                        writeFSMstate_s <= s0_waitForWriteAndSendAddr;
                                                    end if;
            
            end case;
            
        end if;
        
    end process;
    
    init_proc: process(clk_i, reset_i)
        variable cnter:  integer range 0 to (1+((CLK_FREQ *  40000)));
    begin
    
        if reset_i = '1' then
            --! Reset the state machine and counters
            initFSMstate_s       <= s0_delay40ms;            
            cnter               := 0;
            ready_s              <= '0';
            
            E_sInit              <= '0';
            DB_sInit            <= "00000000";
            RS_sInit            <= '0';
            RW_sInit            <= '0';
            activeLSB_sInit     <= '0';
            
        elsif rising_edge(clk_i) then
        
            E_sInit              <= '0';
            
            case initFSMstate_s is
            
                when s0_delay40ms =>        --! Count until counter >= (CLK_FREQ * 380)
                                            cnter := cnter + 1;
                                            
                                            if cnter >= (1+((CLK_FREQ * 1000))) then
                                                initFSMstate_s <= s1_send4bFn;
                                            end if;
                                            
                when s1_send4bFn =>         DB_sInit            <= "00110000";
                                            RS_sInit            <= '0';
                                            RW_sInit            <= '0';
                                            E_sInit             <= '1';
                                            activeLSB_sInit     <= '0';
                                            
                                            cnter               := 0;
                                            
                                            initFSMstate_s <= s2_delay40ms;
                                            
                when s2_delay40ms =>        --! Count until counter >= (CLK_FREQ * 380)
                                            cnter := cnter + 1;
                                            
                                            if cnter >= (1+((CLK_FREQ * 1000))) then
                                                initFSMstate_s <= s3_sendConfig;
                                            end if;
                                                                                            
                when s3_sendConfig =>       DB_sInit            <= "00101100";
                                            RS_sInit            <= '0';
                                            RW_sInit            <= '0';
                                            E_sInit             <= '1';
                                            activeLSB_sInit     <= '1';
                                            
                                            cnter               := 0;
                                            
                                            initFSMstate_s <= s4_delay40ms;
                                            
                when s4_delay40ms =>        --! Count until counter >= (CLK_FREQ * 380)
                                            cnter := cnter + 1;
                                            
                                            if cnter >= (1+((CLK_FREQ * 1000))) then
                                                initFSMstate_s <= s5_sendDisplayOn;
                                            end if;
                                            
                when s5_sendDisplayOn =>    DB_sInit            <= "00001111";
                                            RS_sInit            <= '0';
                                            RW_sInit            <= '0';
                                            E_sInit             <= '1';
                                            activeLSB_sInit     <= '1';
                                            
                                            cnter               := 0;
                                            
                                            initFSMstate_s <= s6_delay40ms;
                                            
                when s6_delay40ms =>        --! Count until counter >= (CLK_FREQ * 380)
                                            cnter := cnter + 1;
                                            
                                            if cnter >= (1+((CLK_FREQ * 1000))) then
                                                initFSMstate_s <= s7_sendClear;
                                            end if;
                                            
                when s7_sendClear =>        DB_sInit            <= "00000001";
                                            RS_sInit            <= '0';
                                            RW_sInit            <= '0';
                                            E_sInit             <= '1';
                                            activeLSB_sInit     <= '1';
                                            
                                            cnter               := 0;
                                            
                                            initFSMstate_s <= s8_wait40ms;
                                            
                when s8_wait40ms =>         --! Count until counter >= (CLK_FREQ * 380)
                                            cnter := cnter + 1;
                                            
                                            if cnter >= (1+((CLK_FREQ * 1000))) then
                                                initFSMstate_s <= s9_ready;
                                            end if;
                                            
                when s9_ready =>            ready_s <= '1';
                
            end case;
            
        end if;
        
   end process;
end;