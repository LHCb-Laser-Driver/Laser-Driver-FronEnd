-------------------------------------------------------
--! @file
--! @author Bruno Allongue (CERN EP-ESE-FE)
--! @date November, 2017
--! @version 1.0
--! @brief LCD Controller - Init state machine and data management
-------------------------------------------------------

--! Include the IEEE VHDL standard library
library ieee;
--! Use STD_Logic to define vector types
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Include the work library
library work;
--! Use SCA Package to define specific types (vector arrays)
use work.LCD_PKG.all;

--! @brief LCD_Driver Entity - Driver module
entity LCDTop is
    port (
        clk_i               : in std_logic;
        reset_i             : in std_logic;
            
        line1               : in LCDLine_arr (0 to 15);
        line2               : in LCDLine_arr (0 to 15);
        Write_i             : in  std_logic;
        
        Rdy_o               : out std_logic;
        
        LCDPosition_o       : out std_logic_vector(6 downto 0);
        LCDCharcater_o      : out std_logic_vector(7 downto 0);
        Write_o             : out std_logic;
        WriteRdy_i          : in  std_logic
    );   
end LCDTop;

--! @brief LCD_Driver Architecture - Driver module
architecture behaviour of LCDTop is

    -- Types
    type sendLineFSM_t is ( s0_waitForWrite,
                            s1_sendLine1_char,
                            s1_2_WaitForOngoing,
                            s2_sendLine1_waitForReady,
                            s3_sendLine2_char,
                            s3_4_WaitForOngoing,
                            s4_sendLine2_waitForReady);
    
    -- Signal
    signal sendLineFSM_s       : sendLineFSM_t;
    signal Write_s0            : std_logic;
    
    signal pos: integer range 0 to 16;
begin       --========####   Architecture Body   ####========-- 
  
    write_proc: process(clk_i, reset_i)
    begin
    
        if reset_i = '1' then
        
            Write_s0 <= Write_i;
            Rdy_o    <= '0';
            pos      <= 0;
            
        elsif rising_edge(clk_i) then
        
            Write_s0 <= Write_i;
            Write_o  <= '0';
            
            case sendLineFSM_s is 
            
               when s0_waitForWrite =>           Rdy_o    <= '1';
                                                 if Write_i = '1' and Write_s0 = '0' then
                                                    Rdy_o         <= '0';
                                                    sendLineFSM_s <= s2_sendLine1_waitForReady;
                                                    pos           <= 0;
                                                 end if;
                                                 
               when s1_sendLine1_char =>         LCDPosition_o <= std_logic_vector(to_unsigned(pos, 7));
                                                 LCDCharcater_o <= characterToHexa(line1(pos));
                                                 Write_o        <= '1';
                                                 
                                                 pos <= pos + 1;
                                                 sendLineFSM_s <= s1_2_WaitForOngoing;
               
               when s1_2_WaitForOngoing =>       if WriteRdy_i = '0' then
                                                    sendLineFSM_s <= s2_sendLine1_waitForReady;
                                                 end if;
                                                             
               when s2_sendLine1_waitForReady => if WriteRdy_i = '1' then
               
                                                    if pos >= 16 then
                                                        pos <= 0;
                                                        sendLineFSM_s <= s3_sendLine2_char;
                                                        
                                                    else
                                                        sendLineFSM_s <= s1_sendLine1_char;
                                                    
                                                    end if;
                                                    
                                                 end if;
                                                 
               when s3_sendLine2_char =>         LCDPosition_o <= std_logic_vector(to_unsigned(pos, 7));
                                                 LCDPosition_o(6) <= '1';
                                                 
                                                 LCDCharcater_o <= characterToHexa(line2(pos));
                                                 Write_o        <= '1';
                                                  
                                                 pos <= pos + 1;
                                                 sendLineFSM_s <= s3_4_WaitForOngoing;
                                                                
               when s3_4_WaitForOngoing =>       if WriteRdy_i = '0' then
                                                    sendLineFSM_s <= s4_sendLine2_waitForReady;
                                                 end if;
                                                 
               when s4_sendLine2_waitForReady => if WriteRdy_i = '1' then
                                                                
                                                     if pos >= 16 then
                                                         pos <= 0;
                                                         sendLineFSM_s <= s0_waitForWrite;
                                                         
                                                     else
                                                         sendLineFSM_s <= s3_sendLine2_char;
                                                     
                                                     end if;
                                                     
                                                  end if;
            
            end case;
            
        end if;
        
    end process;
end;