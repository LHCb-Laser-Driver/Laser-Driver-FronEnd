onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+tx_pll -L unisims_ver -L unimacro_ver -L secureip -L xil_defaultlib -L xpm -O5 xil_defaultlib.tx_pll xil_defaultlib.glbl

do {wave.do}

view wave
view structure
view signals

do {tx_pll.udo}

run -all

endsim

quit -force
