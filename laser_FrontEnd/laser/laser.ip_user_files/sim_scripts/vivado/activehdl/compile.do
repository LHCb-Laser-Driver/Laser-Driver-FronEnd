vlib work
vlib activehdl

vlib activehdl/xil_defaultlib
vlib activehdl/xpm

vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm

vlog -work xil_defaultlib -v2k5 -sv "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/vio_v3_0_12/hdl" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/xsdbs_v1_0_2/hdl/verilog" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/vio_v3_0_12/hdl" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/xsdbs_v1_0_2/hdl/verilog" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_base.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_dpdistram.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_dprom.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_sdpram.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_spram.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_sprom.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_tdpram.sv" \

vcom -work xpm -93 \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -v2k5 "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/vio_v3_0_12/hdl" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/xsdbs_v1_0_2/hdl/verilog" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/vio_v3_0_12/hdl" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/xsdbs_v1_0_2/hdl/verilog" \
"../../../../../../gbt_fpga/example_designs/xilinx_k7v7/core_sources/chipscope_vio/vivado/sim/xlx_k7v7_vio.v" \

vlog -work xil_defaultlib "glbl.v"

