vlib work
vlib riviera

vlib riviera/xil_defaultlib
vlib riviera/xpm

vmap xil_defaultlib riviera/xil_defaultlib
vmap xpm riviera/xpm

vlog -work xil_defaultlib -v2k5 -sv "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/ila_v6_1_1/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/xsdbs_v1_0_2/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/ila_v6_1_1/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/xsdbs_v1_0_2/hdl/verilog" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_base.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_dpdistram.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_dprom.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_sdpram.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_spram.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_sprom.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_tdpram.sv" \

vcom -work xpm -93 \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -v2k5 "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/ila_v6_1_1/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/xsdbs_v1_0_2/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/ila_v6_1_1/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/lcd_ila/xsdbs_v1_0_2/hdl/verilog" \
"../../../../laser.srcs/sources_1/ip/lcd_ila/sim/lcd_ila.v" \

vlog -work xil_defaultlib "glbl.v"

