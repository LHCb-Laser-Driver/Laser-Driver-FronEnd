onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib elink_ila_opt

do {wave.do}

view wave
view structure
view signals

do {elink_ila.udo}

run -all

quit -force
