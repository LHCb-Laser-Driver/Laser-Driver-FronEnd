vlib work
vlib activehdl

vlib activehdl/xil_defaultlib
vlib activehdl/xpm

vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm

vlog -work xil_defaultlib -v2k5 -sv "+incdir+../../../ipstatic/clk_wiz_v5_3_1" "+incdir+../../../ipstatic/clk_wiz_v5_3_1" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_base.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_dpdistram.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_dprom.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_sdpram.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_spram.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_sprom.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_tdpram.sv" \

vcom -work xpm -93 \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -v2k5 "+incdir+../../../ipstatic/clk_wiz_v5_3_1" "+incdir+../../../ipstatic/clk_wiz_v5_3_1" \
"../../../../laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm_clk_wiz.v" \
"../../../../laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm.v" \

vlog -work xil_defaultlib "glbl.v"

