vlib work
vlib msim

vlib msim/xil_defaultlib
vlib msim/xpm

vmap xil_defaultlib msim/xil_defaultlib
vmap xpm msim/xpm

vlog -work xil_defaultlib -64 -incr -sv "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/vio_v3_0_12/hdl" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/xsdbs_v1_0_2/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/vio_v3_0_12/hdl" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/xsdbs_v1_0_2/hdl/verilog" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_base.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_dpdistram.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_dprom.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_sdpram.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_spram.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_sprom.sv" \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_memory/hdl/xpm_memory_tdpram.sv" \

vcom -work xpm -64 -93 \
"C:/EDA/Xilinx/v2016_2/Vivado/2016.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/vio_v3_0_12/hdl" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/xsdbs_v1_0_2/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/ltlib_v1_0_0/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/vio_v3_0_12/hdl" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/xsdbm_v1_1_3/hdl/verilog" "+incdir+../../../../laser.srcs/sources_1/ip/vio_lcd/xsdbs_v1_0_2/hdl/verilog" \
"../../../../laser.srcs/sources_1/ip/vio_lcd/sim/vio_lcd.v" \

vlog -work xil_defaultlib "glbl.v"

