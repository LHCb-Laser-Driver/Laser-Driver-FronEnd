onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+PLL_for_elink_ILA -L unisims_ver -L unimacro_ver -L secureip -L xil_defaultlib -L xpm -O5 xil_defaultlib.PLL_for_elink_ILA xil_defaultlib.glbl

do {wave.do}

view wave
view structure
view signals

do {PLL_for_elink_ILA.udo}

run -all

endsim

quit -force
