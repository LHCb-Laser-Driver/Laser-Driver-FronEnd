onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib PLL_for_elink_ILA_opt

do {wave.do}

view wave
view structure
view signals

do {PLL_for_elink_ILA.udo}

run -all

quit -force
