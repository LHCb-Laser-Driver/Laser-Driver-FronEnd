-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.2 (win64) Build 1577090 Thu Jun  2 16:32:40 MDT 2016
-- Date        : Thu Jul 20 17:23:48 2017
-- Host        : pcepesedesk02 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               C:/ken/xilinx/laser/laser.xpr/laser/laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm_stub.vhdl
-- Design      : xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k325tffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm is
  Port ( 
    clk_in1 : in STD_LOGIC;
    clk_out1 : out STD_LOGIC;
    psclk : in STD_LOGIC;
    psen : in STD_LOGIC;
    psincdec : in STD_LOGIC;
    psdone : out STD_LOGIC;
    reset : in STD_LOGIC;
    locked : out STD_LOGIC
  );

end xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm;

architecture stub of xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_in1,clk_out1,psclk,psen,psincdec,psdone,reset,locked";
begin
end;
