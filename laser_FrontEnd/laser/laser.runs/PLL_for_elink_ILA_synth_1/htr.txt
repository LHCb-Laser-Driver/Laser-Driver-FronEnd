REM
REM Vivado(TM)
REM htr.txt: a Vivado-generated description of how-to-repeat the
REM          the basic steps of a run.  Note that runme.bat/sh needs
REM          to be invoked for Vivado to track run status.
REM Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
REM

vivado -log PLL_for_elink_ILA.vds -m64 -mode batch -messageDb vivado.pb -notrace -source PLL_for_elink_ILA.tcl
