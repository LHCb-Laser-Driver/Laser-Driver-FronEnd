# 
# Synthesis run script generated by Vivado
# 

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
create_project -in_memory -part xc7k325tffg900-2

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.cache/wt [current_project]
set_property parent.project_path C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.xpr [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language Verilog [current_project]
set_property board_part xilinx.com:kc705:part0:1.3 [current_project]
read_ip -quiet c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila.xci
set_property is_locked true [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila.xci]

foreach dcp [get_files -quiet -all *.dcp] {
  set_property used_in_implementation false $dcp
}
read_xdc dont_touch.xdc
set_property used_in_implementation false [get_files dont_touch.xdc]

synth_design -top elink_ila -part xc7k325tffg900-2 -mode out_of_context

rename_ref -prefix_all elink_ila_

write_checkpoint -force -noxdef elink_ila.dcp

catch { report_utilization -file elink_ila_utilization_synth.rpt -pb elink_ila_utilization_synth.pb }

if { [catch {
  file copy -force C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.runs/elink_ila_synth_1/elink_ila.dcp c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila.dcp
} _RESULT ] } { 
  send_msg_id runtcl-3 error "ERROR: Unable to successfully create or copy the sub-design checkpoint file."
  error "ERROR: Unable to successfully create or copy the sub-design checkpoint file."
}

if { [catch {
  write_verilog -force -mode synth_stub c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila_stub.v
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create a Verilog synthesis stub for the sub-design. This may lead to errors in top level synthesis of the design. Error reported: $_RESULT"
}

if { [catch {
  write_vhdl -force -mode synth_stub c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila_stub.vhdl
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create a VHDL synthesis stub for the sub-design. This may lead to errors in top level synthesis of the design. Error reported: $_RESULT"
}

if { [catch {
  write_verilog -force -mode funcsim c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila_sim_netlist.v
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create the Verilog functional simulation sub-design file. Post-Synthesis Functional Simulation with this file may not be possible or may give incorrect results. Error reported: $_RESULT"
}

if { [catch {
  write_vhdl -force -mode funcsim c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila_sim_netlist.vhdl
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create the VHDL functional simulation sub-design file. Post-Synthesis Functional Simulation with this file may not be possible or may give incorrect results. Error reported: $_RESULT"
}

if {[file isdir C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.ip_user_files/ip/elink_ila]} {
  catch { 
    file copy -force c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila_stub.v C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.ip_user_files/ip/elink_ila
  }
}

if {[file isdir C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.ip_user_files/ip/elink_ila]} {
  catch { 
    file copy -force c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila_stub.vhdl C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.ip_user_files/ip/elink_ila
  }
}
