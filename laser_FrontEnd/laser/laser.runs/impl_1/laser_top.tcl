proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  set_param xicom.use_bs_reader 1
  create_project -in_memory -part xc7k325tffg900-2
  set_property board_part xilinx.com:kc705:part0:1.3 [current_project]
  set_property design_mode GateLvl [current_fileset]
  set_param project.singleFileAddWarning.threshold 0
  set_property webtalk.parent_dir C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.cache/wt [current_project]
  set_property parent.project_path C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.xpr [current_project]
  set_property ip_repo_paths c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.cache/ip [current_project]
  set_property ip_output_repo c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.cache/ip [current_project]
  set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
  add_files -quiet C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.runs/synth_1/laser_top.dcp
  add_files -quiet c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/tx_pll/tx_pll.dcp
  set_property netlist_only true [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/tx_pll/tx_pll.dcp]
  add_files -quiet c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm.dcp
  set_property netlist_only true [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm.dcp]
  add_files -quiet c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_rx_dpram/xlx_k7v7_rx_dpram.dcp
  set_property netlist_only true [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_rx_dpram/xlx_k7v7_rx_dpram.dcp]
  add_files -quiet c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_tx_dpram/xlx_k7v7_tx_dpram.dcp
  set_property netlist_only true [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_tx_dpram/xlx_k7v7_tx_dpram.dcp]
  add_files -quiet c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vivado_debug/ip/xlx_k7v7_vivado_debug/xlx_k7v7_vivado_debug.dcp
  set_property netlist_only true [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vivado_debug/ip/xlx_k7v7_vivado_debug/xlx_k7v7_vivado_debug.dcp]
  add_files -quiet c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vio/ip/xlx_k7v7_vio/xlx_k7v7_vio.dcp
  set_property netlist_only true [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vio/ip/xlx_k7v7_vio/xlx_k7v7_vio.dcp]
  add_files -quiet c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/PLL_for_elink_ILA/PLL_for_elink_ILA.dcp
  set_property netlist_only true [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/PLL_for_elink_ILA/PLL_for_elink_ILA.dcp]
  add_files -quiet c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/phase_shift_PLL/phase_shift_PLL.dcp
  set_property netlist_only true [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/phase_shift_PLL/phase_shift_PLL.dcp]
  add_files -quiet c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/vio_lcd/vio_lcd.dcp
  set_property netlist_only true [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/vio_lcd/vio_lcd.dcp]
  add_files -quiet c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/lcd_ila/lcd_ila.dcp
  set_property netlist_only true [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/lcd_ila/lcd_ila.dcp]
  add_files -quiet c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila.dcp
  set_property netlist_only true [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila.dcp]
  read_xdc -mode out_of_context -ref tx_pll -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/tx_pll/tx_pll_ooc.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/tx_pll/tx_pll_ooc.xdc]
  read_xdc -prop_thru_buffers -ref tx_pll -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/tx_pll/tx_pll_board.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/tx_pll/tx_pll_board.xdc]
  read_xdc -ref tx_pll -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/tx_pll/tx_pll.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/tx_pll/tx_pll.xdc]
  read_xdc -mode out_of_context -ref xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm_ooc.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm_ooc.xdc]
  read_xdc -prop_thru_buffers -ref xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm_board.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm_board.xdc]
  read_xdc -ref xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm.xdc]
  read_xdc -mode out_of_context -ref xlx_k7v7_rx_dpram -cells U0 c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_rx_dpram/xlx_k7v7_rx_dpram_ooc.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_rx_dpram/xlx_k7v7_rx_dpram_ooc.xdc]
  read_xdc -mode out_of_context -ref xlx_k7v7_tx_dpram -cells U0 c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_tx_dpram/xlx_k7v7_tx_dpram_ooc.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_tx_dpram/xlx_k7v7_tx_dpram_ooc.xdc]
  read_xdc -mode out_of_context -ref xlx_k7v7_vivado_debug -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vivado_debug/ip/xlx_k7v7_vivado_debug/xlx_k7v7_vivado_debug_ooc.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vivado_debug/ip/xlx_k7v7_vivado_debug/xlx_k7v7_vivado_debug_ooc.xdc]
  read_xdc -ref xlx_k7v7_vivado_debug -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vivado_debug/ip/xlx_k7v7_vivado_debug/ila_v6_1/constraints/ila.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vivado_debug/ip/xlx_k7v7_vivado_debug/ila_v6_1/constraints/ila.xdc]
  read_xdc -mode out_of_context -ref xlx_k7v7_vio c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vio/ip/xlx_k7v7_vio/xlx_k7v7_vio_ooc.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vio/ip/xlx_k7v7_vio/xlx_k7v7_vio_ooc.xdc]
  read_xdc -ref xlx_k7v7_vio c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vio/ip/xlx_k7v7_vio/xlx_k7v7_vio.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/xlx_k7v7_vio/ip/xlx_k7v7_vio/xlx_k7v7_vio.xdc]
  read_xdc -mode out_of_context -ref PLL_for_elink_ILA -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/PLL_for_elink_ILA/PLL_for_elink_ILA_ooc.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/PLL_for_elink_ILA/PLL_for_elink_ILA_ooc.xdc]
  read_xdc -prop_thru_buffers -ref PLL_for_elink_ILA -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/PLL_for_elink_ILA/PLL_for_elink_ILA_board.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/PLL_for_elink_ILA/PLL_for_elink_ILA_board.xdc]
  read_xdc -ref PLL_for_elink_ILA -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/PLL_for_elink_ILA/PLL_for_elink_ILA.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/PLL_for_elink_ILA/PLL_for_elink_ILA.xdc]
  read_xdc -mode out_of_context -ref phase_shift_PLL -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/phase_shift_PLL/phase_shift_PLL_ooc.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/phase_shift_PLL/phase_shift_PLL_ooc.xdc]
  read_xdc -prop_thru_buffers -ref phase_shift_PLL -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/phase_shift_PLL/phase_shift_PLL_board.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/phase_shift_PLL/phase_shift_PLL_board.xdc]
  read_xdc -ref phase_shift_PLL -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/phase_shift_PLL/phase_shift_PLL.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/phase_shift_PLL/phase_shift_PLL.xdc]
  read_xdc -mode out_of_context -ref vio_lcd c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/vio_lcd/vio_lcd_ooc.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/vio_lcd/vio_lcd_ooc.xdc]
  read_xdc -ref vio_lcd c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/vio_lcd/vio_lcd.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/vio_lcd/vio_lcd.xdc]
  read_xdc -mode out_of_context -ref lcd_ila -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/lcd_ila/lcd_ila_ooc.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/lcd_ila/lcd_ila_ooc.xdc]
  read_xdc -ref lcd_ila -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/lcd_ila/ila_v6_1/constraints/ila.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/lcd_ila/ila_v6_1/constraints/ila.xdc]
  read_xdc -mode out_of_context -ref elink_ila -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila_ooc.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/elink_ila_ooc.xdc]
  read_xdc -ref elink_ila -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/ila_v6_1/constraints/ila.xdc
  set_property processing_order EARLY [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/elink_ila/ila_v6_1/constraints/ila.xdc]
  read_xdc C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/constrs_1/new/laser.xdc
  read_xdc C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/constrs_1/new/laser_floorplanning.xdc
  read_xdc C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/constrs_1/new/laser_clks.xdc
  read_xdc C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/constrs_1/new/laser_timingclosure.xdc
  read_xdc C:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/constrs_1/new/laser_implementation.xdc
  read_xdc -ref xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm -cells inst c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm_late.xdc
  set_property processing_order LATE [get_files c:/ken/xilinx/laser/laser_FrontEnd/laser/laser.srcs/sources_1/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm_late.xdc]
  link_design -top laser_top -part xc7k325tffg900-2
  write_hwdef -file laser_top.hwdef
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step opt_design
set rc [catch {
  create_msg_db opt_design.pb
  opt_design 
  write_checkpoint -force laser_top_opt.dcp
  report_drc -file laser_top_drc_opted.rpt
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  implement_debug_core 
  place_design 
  write_checkpoint -force laser_top_placed.dcp
  report_io -file laser_top_io_placed.rpt
  report_utilization -file laser_top_utilization_placed.rpt -pb laser_top_utilization_placed.pb
  report_control_sets -verbose -file laser_top_control_sets_placed.rpt
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force laser_top_routed.dcp
  report_drc -file laser_top_drc_routed.rpt -pb laser_top_drc_routed.pb
  report_timing_summary -warn_on_violation -max_paths 10 -file laser_top_timing_summary_routed.rpt -rpx laser_top_timing_summary_routed.rpx
  report_power -file laser_top_power_routed.rpt -pb laser_top_power_summary_routed.pb -rpx laser_top_power_routed.rpx
  report_route_status -file laser_top_route_status.rpt -pb laser_top_route_status.pb
  report_clock_utilization -file laser_top_clock_utilization_routed.rpt
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

start_step write_bitstream
set rc [catch {
  create_msg_db write_bitstream.pb
  catch { write_mem_info -force laser_top.mmi }
  write_bitstream -force laser_top.bit 
  catch { write_sysdef -hwdef laser_top.hwdef -bitfile laser_top.bit -meminfo laser_top.mmi -file laser_top.sysdef }
  catch {write_debug_probes -quiet -force debug_nets}
  close_msg_db -file write_bitstream.pb
} RESULT]
if {$rc} {
  step_failed write_bitstream
  return -code error $RESULT
} else {
  end_step write_bitstream
}

