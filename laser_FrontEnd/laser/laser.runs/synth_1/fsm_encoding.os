
 add_fsm_encoding \
       {State_Display.StateDisplayFSM_s} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {LCDTop.sendLineFSM_s} \
       { }  \
       {{000 000} {001 101} {010 110} {011 001} {100 010} {101 011} {110 100} }

 add_fsm_encoding \
       {LCDCtrller.initFSMstate_s} \
       { }  \
       {{0000 0000000001} {0001 0000000010} {0010 0000000100} {0011 0000001000} {0100 0000010000} {0101 0000100000} {0110 0001000000} {0111 0010000000} {1000 0100000000} {1001 1000000000} }

 add_fsm_encoding \
       {LCD_Driver.driverstate_s} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {phase_shift_control.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }
