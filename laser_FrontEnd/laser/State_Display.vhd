-------------------------------------------------------
--! @file
--! @author Bruno Allongue (CERN EP-ESE-FE)
--! @date November, 2017
--! @version 1.0
--! @brief LCD Controller - Init state machine and data management
-------------------------------------------------------

--! Include the IEEE VHDL standard library
library ieee;
--! Use STD_Logic to define vector types
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Include the work library
library work;
--! Use SCA Package to define specific types (vector arrays)
use work.LCD_PKG.all;

--! @brief LCD_Driver Entity - Driver module
entity State_Display is
    port (
        clk_i               : in std_logic;
        reset_i             : in std_logic;
            
        line1               : out LCDLine_arr (0 to 15);
        line2               : out LCDLine_arr (0 to 15);
        Write_o             : out  std_logic;
        
        LCDRdy_i            : in std_logic;
        
        State1              : in std_logic;
        State2              : in std_logic;
        State3              : in std_logic;
        out0                : in std_logic_vector (7 downto 0);
        out1               : in std_logic_vector (7 downto 0);
        out2               : in std_logic_vector (7 downto 0);
        out3               : in std_logic_vector (7 downto 0)
    );   
end State_Display;

--! @brief LCD_Driver Architecture - Driver module
architecture behaviour of State_Display is

    -- Types
    type StateDisplayFSM_t is ( s0_checkState1,
                                s0_WaitLCDRdy,
                                s0_LCDdisplay);
                                
--                                s1_checkState2,
--                                s1_WaitLCDRdy,
--                                s1_LCDdisplay,
--                                s2_checkState3,
--                                s2_WaitLCDRdy,
--                                s2_LCDdisplay);
    
    -- Signal
    signal StateDisplayFSM_s       : StateDisplayFSM_t;
    
    signal out0_r                  : std_logic_vector(7 downto 0);
    signal out1_r                  : std_logic_vector(7 downto 0);
    signal out2_r                  : std_logic_vector(7 downto 0);
    signal out3_r                  : std_logic_vector(7 downto 0);
    
begin       --========####   Architecture Body   ####========-- 
  
    LCDdispaly_proc: process(clk_i, reset_i)
    begin
    
        if reset_i = '1' then
        
            StateDisplayFSM_s <= s0_checkState1;
             line1  <= "                ";
             line2  <= "                ";
             Write_o <= '0';
            
             out0_r <= "11111111";
             out1_r <= "11111111";
             out2_r <= "11111111";
             out3_r <= "11111111";
             
        elsif rising_edge(clk_i) then
                          
            case StateDisplayFSM_s  is 
            
               when s0_checkState1 => if State1 = '1' and (out0_r /= out0 or out1_r /= out1 or out2_r /= out2 or out3_r /= out3) then
                                        StateDisplayFSM_s <= s0_WaitLCDRdy;  
                                        
                                        out0_r <= out0;
                                        out1_r <= out1;
                                        out2_r <= out2;
                                        out3_r <= out3;
                                        
                                      end if;
                                      
               when s0_WaitLCDRdy  => if  LCDRdy_i = '1' then
                                      StateDisplayFSM_s <= s0_LCDdisplay;
                                      end if;
                                      
               when s0_LCDdisplay  => line1  <= "o0-" & HexToASCII(out0) & "h o1-" & HexToASCII(out1) & "h   ";
                                      line2  <= "o2-" & HexToASCII(out2) & "h o3-" & HexToASCII(out3) & "h   ";                                         Write_o <= '1';
                                      
                                      if LCDRdy_i = '0' then
                                        StateDisplayFSM_s <= s0_checkState1 ;
                                        Write_o <= '0';
                                      end if;
               --when s1_checkState2 =>
               --when s1_WaitLCDRdy  =>
               --when s1_LCDdisplay  =>
               --when s2_checkState3 =>
               --when s2_WaitLCDRdy  =>
               --when s2_LCDdisplay  =>
            
            end case;
            
        end if;
        
    end process;
end;