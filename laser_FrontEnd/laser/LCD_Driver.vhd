-------------------------------------------------------
--! @file
--! @author Bruno Allongue (CERN EP-ESE-FE)
--! @date November, 2017
--! @version 1.0
--! @brief LCD Display - Driver module
-------------------------------------------------------

--! Include the IEEE VHDL standard library
library ieee;
--! Use STD_Logic to define vector types
use ieee.std_logic_1164.all;

--! @brief LCD_Driver Entity - Driver module
entity LCD_Driver is
    generic (
        CLK_FREQ            : integer
    );
    port (
        reset_i             : in  std_logic;
        clk_i               : in  std_logic;                        --! Clock used for the internal state machine
        
        DB_i                : in  std_logic_vector(7 downto 0);     --! DB bus (LCD pins)
        RS_i                : in  std_logic;                        --! Register select (0: Instruction reg / 1: Data register)
        RW_i                : in  std_logic;                        --! Read/Write command (0: Write / 1: Read)
        E_i                 : in  std_logic;                        --! Start command (rising edge detection)
        activeLSB_i         : in  std_logic;
        
        LCD_DB4_LS          : out std_logic;
        LCD_DB5_LS          : out std_logic;
        LCD_DB6_LS          : out std_logic;
        LCD_DB7_LS          : out std_logic;
        LCD_RW_LS           : out std_logic;
        LCD_RS_LS           : out std_logic;
        LCD_E_LS            : out std_logic
    );   
end LCD_Driver;

--! @brief LCD_Driver Architecture - Driver module
architecture behaviour of LCD_Driver is

    -- Types
    type driverstate_t is ( s0_setRSRW, 
                            s1_SetEToHigh, 
                            s2_WaitBeforeData, 
                            s3_SetDATA, 
                            s4_WaitBeforeRelease,
                            s5_ClearE,
                            s6_WaitForEnd);
                            
    
    -- Signal
    signal driverstate_s        : driverstate_t;
    signal E_i_s0               : std_logic;
    
    signal offset_s             : std_logic;
    
begin       --========####   Architecture Body   ####========-- 

    driver_proc: process(clk_i, reset_i)
        variable cnter:  integer range 0 to (1+((CLK_FREQ *  4)/10));
    begin
    
        if reset_i = '1' then
            --! Reset the state machine and counters
            driverstate_s       <= s0_setRSRW;
            E_i_s0              <= E_i;
            offset_s            <= '0';
            
            cnter               := 0;
            
        elsif rising_edge(clk_i) then
            
            E_i_s0 <= E_i;
            
            --! Execute a task from the state machine
            case driverstate_s is
            
                when s0_setRSRW             =>  if E_i = '1' and E_i_s0 = '0' then
                                            
                                                    driverstate_s <= s1_SetEToHigh;
                                                    offset_s      <= '0';
                                        
                                                end if;
                                        
                when s1_SetEToHigh          =>  LCD_RS_LS <= RS_i;
                                                LCD_RW_LS <= RW_i;
                                                LCD_E_LS <= '1';
                                                
                                                cnter := 0;
                                                
                                                driverstate_s <= s2_WaitBeforeData;
                                        
                when s2_WaitBeforeData      =>  --! Count until counter >= (CLK_FREQ * 380)
                                                cnter := cnter + 1;
                                                
                                                if cnter >= (1+((CLK_FREQ * 4)/10)) then
                                                    driverstate_s <= s3_SetDATA;
                                                end if;
                                                
                when s3_SetDATA             =>  if offset_s = '0' then
                                                
                                                    LCD_DB4_LS          <= DB_i(4);
                                                    LCD_DB5_LS          <= DB_i(5);
                                                    LCD_DB6_LS          <= DB_i(6);
                                                    LCD_DB7_LS          <= DB_i(7);
                                                    
                                                else
                                                
                                                    LCD_DB4_LS          <= DB_i(0);
                                                    LCD_DB5_LS          <= DB_i(1);
                                                    LCD_DB6_LS          <= DB_i(2);
                                                    LCD_DB7_LS          <= DB_i(3);                                                
                                                
                                                end if;
                                                
                                                cnter := 0;
                                                
                                                driverstate_s <= s4_WaitBeforeRelease;
                
                when s4_WaitBeforeRelease   => --! Count until counter >= (CLK_FREQ * 80)
                                                cnter := cnter + 1;
                                                
                                                if cnter >= (1+((CLK_FREQ * 1)/10)) then
                                                    driverstate_s <= s5_ClearE;
                                                end if;
                
                when s5_ClearE              =>  LCD_E_LS <= '0';
                                                
                                                cnter := 0;
                                                
                                                driverstate_s <= s6_WaitForEnd;
                
                when s6_WaitForEnd          => --! Count until counter >= (CLK_FREQ * 10)
                                                cnter := cnter + 1;
                                                
                                                if cnter >= (1+((CLK_FREQ * 1)/10)) then
                                                    if offset_s = '0' and activeLSB_i = '1' then
                                                      offset_s <= '1';
                                                      driverstate_s <= s1_SetEToHigh;
                                                      
                                                    else 
                                                      driverstate_s <= s0_setRSRW;
                                                      
                                                    end if;
                                                end if;
                                               
            end case;
            
        
        
        end if;
    
    end process;
	 
    
end behaviour;
--============================================================================--
--############################################################################--
--============================================================================--